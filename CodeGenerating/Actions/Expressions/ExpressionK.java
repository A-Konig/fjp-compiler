package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Expressions.Operations.PrefixOP;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Type K Expression
 *
 * @author kacerekz
 */
public class ExpressionK extends Expression {

    /** Operation */
    public PrefixOP op;

    /** Expression */
    public ExpressionF exp;

    /**
     * Generate code to execute this expression
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        if (op == null) {
            exp.executeAction(null);
            BrainfuckController.append(exp.getActionCode(), 1);

        } else {
            op.executeAction(null);
            BrainfuckController.append(op.getActionCode(), 1);
        }
    }

    /**
     * Set operation
     * @param op
     */
    @Override
    public void setPrefixOP(PrefixOP op) {
        this.op = op;
    }

    /**
     * Set expression
     * @param e Type F Expression
     */
    @Override
    public void setExpressionF(ExpressionF e) {
        exp = e;
    }
}
