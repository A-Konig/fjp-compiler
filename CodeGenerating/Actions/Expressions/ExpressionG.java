package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Action;
import CodeGenerating.Actions.Expressions.Values.*;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Type G Expression
 *
 * @author kacerekz
 */
public class ExpressionG extends Expression {

    /** Expression */
    public ExpressionA exp;

    /** Set number value */
    public NumberExpression n;

    /** Set identifier */
    public IdentifierExpression i;

    /** Set boolean value */
    public BooleanExpression b;

    /** Set character value */
    public CharacterExpression c;

    /** Set array expression */
    public ArrayExpression arrayExp;

    /**
     * Generate code to execute this expression
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        String actionCode = null;
        Action a = null;

        if (exp != null)            {a = exp;}
        else if (n != null)         {a = n;}
        else if (i != null)         {a = i;}
        else if (b != null)         {a = b;}
        else if (c != null)         {a = c;}
        else if (arrayExp != null)  {a = arrayExp;}

        if (a != null){
            a.executeAction(null);
            actionCode = a.getActionCode();
        }

        BrainfuckController.append(actionCode, 1);
    }

    /**
     * Set the expression
     * @param e Type A Expression
     */
    @Override
    public void setExpressionA(ExpressionA e) {
        exp = e;
    }

    /**
     * Set number value
     * @param e A number value
     */
    @Override
    public void setNumberExpression(NumberExpression e) {
        n = e;
    }

    /**
     * Set identifier
     * @param e An identifier
     */
    @Override
    public void setIdentifierExpression(IdentifierExpression e) {
        i = e;
    }

    /**
     * Set boolean value
     * @param e A boolean value
     */
    @Override
    public void setBooleanExpression(BooleanExpression e) {
        b = e;
    }

    /**
     * Set character value
     * @param e A character value
     */
    @Override
    public void setCharacterExpression(CharacterExpression e) {
        c = e;
    }

    /**
     * Set array
     * @param e An Array value
     */
    @Override
    public void setArrayExpression(ArrayExpression e) {
        arrayExp = e;
    }
}
