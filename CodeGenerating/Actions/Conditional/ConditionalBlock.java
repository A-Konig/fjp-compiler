package CodeGenerating.Actions.Conditional;

import CodeGenerating.Actions.Action;

/**
 * Represents a block with a condition
 *
 * @author kacerekz
 */
public abstract class ConditionalBlock extends Action {

    /** Every conditional block must have a condition upon which it runs */
    public Condition condition = new Condition();

}
