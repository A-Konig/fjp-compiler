package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Expressions.Operations.PostfixOP;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Type F Expression
 *
 * @author kacerekz
 */
public class ExpressionF extends Expression {

    /** Expression */
    public ExpressionG exp;

    /** Operation */
    public PostfixOP op;

    /**
     * Generate code to execute this expression
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        if (op == null) {
            exp.executeAction(null);
            BrainfuckController.append(exp.getActionCode(), 1);

        } else {
            op.executeAction(null);
            BrainfuckController.append(op.getActionCode(), 1);
        }
    }

    /**
     * Set expression
     * @param e Type G Expression
     */
    @Override
    public void setExpressionG(ExpressionG e) {
        exp = e;
    }

    /**
     * Set operation
     * @param op A Postfix operation
     */
    @Override
    public void setPostfixOP(PostfixOP op) {
        this.op = op;
    }
}
