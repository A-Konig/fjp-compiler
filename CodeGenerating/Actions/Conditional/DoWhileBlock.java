package CodeGenerating.Actions.Conditional;

import CodeGenerating.Controllers.BrainfuckController;

/**
 * Describes and evaluates DoWhile blocks
 *
 * @author kacerekz
 */
public class DoWhileBlock extends ConditionalBlock {

    /**
     * Generate DoWhile block code
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Generate condition code
        condition.executeAction(null);

        // Move to reg:0
        // Set it to 1 (true) to execute the first loop
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("+", 1);

        // Cycle start
        // Null condition result
        // Move to memory:0
        // Execute nested block code
        BrainfuckController.append("[", 1);
        BrainfuckController.append("[-]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.append(nestedCode, 1);

        // Evaluate condition before cycle end
        BrainfuckController.append(condition.getActionCode(), 1);
        BrainfuckController.pop(BrainfuckController.registerStart);
        BrainfuckController.append(">", BrainfuckController.registerStart);

        // Cycle end
        BrainfuckController.append("]", 1);

        // Return to 0 unconditionally
        BrainfuckController.append("<", BrainfuckController.registerStart);
    }
}
