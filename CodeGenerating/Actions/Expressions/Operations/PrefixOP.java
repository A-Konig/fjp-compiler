package CodeGenerating.Actions.Expressions.Operations;

import CodeGenerating.Actions.Action;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.OPTypePrefix;
import Main.Variable;

/**
 * Represents a prefix operation
 */
public class PrefixOP extends Action {

    /** Operation type */
    public OPTypePrefix type;

    /** Variable */
    public Variable variable;

    /**
     * Create a prefix operation
     * @param type Operation type
     */
    public PrefixOP(OPTypePrefix type){
        this.type = type;
    }

    /**
     * Execute prefix operation
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // First move to variable and change it
        BrainfuckController.append(">", variable.location);
        BrainfuckController.append((type == OPTypePrefix.OP_ADD) ? "+" : "-", 1);
        BrainfuckController.append("<", variable.location);

        // Then push the result
        BrainfuckController.copy(variable.location, BrainfuckController.registerStart, BrainfuckController.registerStart + 1);
        BrainfuckController.push(BrainfuckController.registerStart);
    }

    /**
     * Set variable
     * @param variable Variable
     */
    public void setVariable(Variable variable){
        this.variable = variable;
    }

}
