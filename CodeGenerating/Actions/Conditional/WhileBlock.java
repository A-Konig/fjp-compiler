package CodeGenerating.Actions.Conditional;

import CodeGenerating.Controllers.BrainfuckController;

/**
 * Describes and evaluates While blocks
 *
 * @author kacerekz
 */
public class WhileBlock extends ConditionalBlock {

    /**
     * Generate WhileBlock code
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Process condition and append generated code
        condition.executeAction(null);
        BrainfuckController.append(condition.getActionCode(), 1);

        // Condition result is on the stack
        // Pop result to reg0
        // Move to reg0
        BrainfuckController.pop(BrainfuckController.registerStart);
        BrainfuckController.append(">", BrainfuckController.registerStart);

        // Cycle start
        // Null condition result
        // Move to memory:0
        // Execute block code
        BrainfuckController.append("[", 1);
        BrainfuckController.append("[-]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.append(nestedCode, 1);

        // Evaluate condition before cycle end
        BrainfuckController.append(condition.getActionCode(), 1);
        BrainfuckController.pop(BrainfuckController.registerStart);
        BrainfuckController.append(">", BrainfuckController.registerStart);

        // Cycle end
        BrainfuckController.append("]", 1);

        // Return to 0 unconditionally
        BrainfuckController.append("<", BrainfuckController.registerStart);
    }

}
