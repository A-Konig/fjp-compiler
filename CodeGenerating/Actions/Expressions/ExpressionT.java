package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Expressions.Operations.ArithmeticOP2;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Type T Expression
 *
 * @author kacerekz
 */
public class ExpressionT extends Expression {

    /** Left side expression */
    public ExpressionT left;

    /** Right side expression */
    public ExpressionK right;

    /** Operation */
    public ArithmeticOP2 op;

    /**
     * Generate code to execute this expression
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        if (op == null){
            right.executeAction(null);
            BrainfuckController.append(right.getActionCode(), 1);

        } else {
            // Evaluate left and right branch
            left.executeAction(null);
            right.executeAction(null);
            BrainfuckController.append(left.getActionCode(), 1);
            BrainfuckController.append(right.getActionCode(), 1);

            // Execute operation
            op.executeAction(null);
            BrainfuckController.append(op.getActionCode(), 1);
        }
    }

    /**
     * Set left side
     * @param e Type T Expression
     */
    @Override
    public void setExpressionT(ExpressionT e) {
        left = e;
    }

    /**
     * Set right side
     * @param e Type K Expression
     */
    @Override
    public void setExpressionK(ExpressionK e) {
        right = e;
    }

    /**
     * Set operation
     * @param op Level 2 Arithmetic operation
     */
    @Override
    public void setArithmeticOP2(ArithmeticOP2 op) {
        this.op = op;
    }
}
