package CodeGenerating.Actions;

import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Controllers.DeclarationController;
import Main.Variable;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a function as an Action
 * Its code is written to its codeTarget during generation
 * Upon execution, it will first clear all of its variables
 * And then append its codeTarget to the current target
 *
 * @author kacerekz
 */
public class Function extends Action {

    /** Function name */
    public String name;

    /** List of parameters */
    public List<Variable> parameters;

    /** In case of getchar */
    public Variable toRead;

    /**
     * Create a function
     */
    public Function(){
        parameters = new ArrayList<>();
    }

    /**
     * Set function name
     * @param name Function name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Executes a function call
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {

        if (name.equals("printf")){
            // Nest passing parameters
            BrainfuckController.append(nestedCode, 1);

            BrainfuckController.append(">", this.parameters.get(0).location);
            BrainfuckController.append(".", 1);
            BrainfuckController.append("[-]", 1);
            BrainfuckController.append("<", this.parameters.get(0).location);

        } else if (name.equals("getchar")){
            BrainfuckController.append(">", toRead.location);
            BrainfuckController.append("[-]", 1);
            BrainfuckController.append(",", 1);
            BrainfuckController.append("<", toRead.location);

        } else {
            // Get list of variables in this function
            List<Variable> variables = DeclarationController.getAllVariables(this);

            // Zero out the variables
            for (Variable v : variables){
                BrainfuckController.append(">", v.location);
                BrainfuckController.append("[-]", 1);
                BrainfuckController.append("<", v.location);
            }

            // Nest passing parameters
            BrainfuckController.append(nestedCode, 1);

            // Append own code
            BrainfuckController.append(getActionCode(), 1);
        }
    }
}
