package CodeGenerating.Actions.Expressions.Values;

import CodeGenerating.Actions.Action;
import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.BrainfuckController;
import Main.Variable;

public class ArrayExpression extends Expression {

    public static String write = ">[>>>[-<<<<+>>>>]<[->+<]<[->+<]<[->+<]>-]>>>[-]<[->+<]<[[-<+>]<<<[->>>>+<<<<]>>-]<<";
    public static String read = ">[>>>[-<<<<+>>>>]<<[->+<]<[->+<]>-]>>>[-<+<<+>>>]<<<[->>>+<<<]>[[-<+>]>[-<+>]<<<<[->>>>+<<<<]>>-]<<";

    public Variable variable;

    public NumberExpression n;
    public IdentifierExpression i;
    public CharacterExpression c;
    public ArrayExpression arrayExp;

    @Override
    public void executeDirectAction(String nestedCode) {
        String actionCode = null;
        Action a = null;

        if (n != null)              {a = n;}
        else if (i != null)         {a = i;}
        else if (c != null)         {a = c;}
        else if (arrayExp != null)  {a = arrayExp;}

        if (a != null){
            a.executeAction(null);
            actionCode = a.getActionCode();
        }

        BrainfuckController.append(actionCode, 1);

        // Index is on the stack
        // Put index in indexer code
        // Retrieve value
        // Copy value to reg:0
        // Push value to stack
        // End at mem:0
    }

    @Override
    public void setNumberExpression(NumberExpression e) {
        n = e;
    }

    @Override
    public void setIdentifierExpression(IdentifierExpression e) {
        i = e;
    }

    @Override
    public void setCharacterExpression(CharacterExpression e) {
        c = e;
    }

    @Override
    public void setArrayExpression(ArrayExpression e) {
        arrayExp = e;
    }
}
