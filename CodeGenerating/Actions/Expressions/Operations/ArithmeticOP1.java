package CodeGenerating.Actions.Expressions.Operations;

import CodeGenerating.Actions.Action;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.OPTypeArithmetic1;

/**
 * Represents a level 1 arithmetic operation - addition or subtraction
 *
 * @author kacerekz
 */
public class ArithmeticOP1 extends Action {

    /** Operation type */
    public OPTypeArithmetic1 type;

    /**
     * Create a level 1 arithmetic operation
     * @param type Operation type
     */
    public ArithmeticOP1(OPTypeArithmetic1 type){
        this.type = type;
    }

    /**
     * Generates appropriate BF algorithm
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // On stack, right is on top
        BrainfuckController.pop(BrainfuckController.registerStart + 1);
        BrainfuckController.pop(BrainfuckController.registerStart + 0);

        switch (type) {
            case OP_ADD:
                BrainfuckController.add();
                break;
            case OP_SUB:
                BrainfuckController.sub();
                break;
        }
    }


}
