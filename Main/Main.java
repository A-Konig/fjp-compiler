package Main;

import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.CodeAnalysis;
import CodeGenerating.Controllers.FlowController;
import Grammar.MicroCLexer;
import Grammar.MicroCListener;
import Grammar.MicroCParser;
import SemanticAnalysis.SemanticListener;
import SemanticAnalysis.VariableListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    private static String filePath = "input/example1.mc";

    /**
     * Load file name from command line parameters
     * @param args
     */
    private static boolean loadFileParameter(String[] args) {

        for (int i = 0; i < args.length - 1; i++) {
            if (args[i].equals("-f")) {
                if(args[i+1].endsWith(".mc")) {
                    filePath = args[i+1];
                    System.out.println("Processing file " + filePath);
                    return true;
                } else {
                    System.out.println("Error while loading file - file of incorrect type");
                    return false;
                }
            }
        }

        System.out.println("No input file");
        return false;
    }


    /**
     * Reads file to string
     * @param filePath  path to file
     * @return          string with file contents
     */
    private static String readFile(String filePath) {
        String content = "";
        try {
            content = new String (Files.readAllBytes(Paths.get(filePath)));
        }
        catch (IOException e) {
            System.out.println("Error while reading file");
            content = null;
            //e.printStackTrace();
        }

        return content;
    }

    /**
     * Starts semantic analysis and checks its outcome
     * @param listener  listener
     * @param tree      parse tree
     * @return          true if executed correctly, false if not
     */
    private static boolean CheckForSemanticErrors(MicroCListener listener, ParseTree tree) {
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);

        ArrayList<String> err = ((SemanticListener) listener).helper.analysator.errors;

        // look for main function
        if (!((SemanticListener) listener).helper.analysator.funcTable.containsKey("main")) {
            err.add("No main function");
        } else {
            // check main function
            Function main = ((SemanticListener) listener).helper.analysator.funcTable.get("main");
            if (main.parameters.size() > 0) {
                err.add("Main function cannot have any parameters");
            }
        }

        // write all errors
        if (!WriteErrors(err))
            return false;

        return true;
    }

    /**
     * Count variables and get their lenghts
     * @param listener  listener
     * @param tree      parse tree
     * @return          true if executed correctly, false if not
     */
    private static boolean GetVariables(MicroCListener listener, ParseTree tree) {
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);

        Hashtable<String, Function> functions = ((VariableListener) listener).counter.funcTable;
        Set<String> varKeys = functions.keySet();

        // check if there are any arrays with length zero
        boolean noError = true;
        for(String key: varKeys){
            Function f = functions.get(key);

            for (int i = 0; i < f.parameters.size(); i++) {
                Variable v = f.parameters.get(i);
                if (v.array && v.arrayLength == 0) {
                    System.out.println("Parameter " + v.name + " is declared with zero length in function " + f.name);
                    noError = false;
                }
            }

            for (int i = 0; i < f.allVariables.size(); i++) {
                Variable v = f.allVariables.get(i);
                if (v.array && v.arrayLength == 0) {
                    System.out.println("Variable " + v.name + " is declared with zero length in function " + f.name);
                    noError = false;
                }
            }
        }


        return noError;
    }

    /**
     * Write out errors
     * @param err   error list
     * @return      false if errors, true if not
     */
    private static boolean WriteErrors(ArrayList<String> err) {
        if (err.size() != 0) {
            for (int i = 0; i < err.size(); i++) {
                System.out.println(err.get(i));
            }

            System.out.println("Syntact analysis failed, please consult the error list.");
            return false;
        }

        return true;
    }

    /**
     * Main method
     * @param args  command line arguments
     */
    public static void main(String[] args) {

        if (args.length > 1)
            if (!loadFileParameter(args))
                return;
        String code = readFile(filePath);
        if (code == null)
            return;

        MicroCLexer lexer = new MicroCLexer(CharStreams.fromString(code));
        MicroCParser parser = new MicroCParser(new CommonTokenStream(lexer));
        ParseTree tree = parser.primaryExpression();

        int errC = parser.getNumberOfSyntaxErrors();
        if (errC > 0) {
            System.out.println("Encountered errors while parsing file. Please consult the error list.");
            return;
        }

        // ----------------------------- SYNTACT ANALYSIS ----------------------------------

        // Syntact analysis
        SemanticListener semListener = new SemanticListener(lexer);
        if (!CheckForSemanticErrors(semListener, tree)) {
            return;
        }

        // Pass for counting number of variables in functions and their type
        VariableListener varListener = new VariableListener(lexer);
        if (!GetVariables(varListener, tree)) {
            System.out.println("Error in declared variables. Consult the error list.");
            return;
        }

        System.out.println("Syntact analysis done");

        // ------------------------------ CODE GENERATION ----------------------------------

        Hashtable<String, Function> functions = ((VariableListener) varListener).counter.funcTable;
        Set<String> varKeys = functions.keySet();

        int paramsize = 1;

        for(String key: varKeys){
            Function f = functions.get(key);
            // System.out.println("Function: " + key);

            // System.out.println("- parameters");
            for (int i = 0; i < f.parameters.size(); i++) {
                Variable v = f.parameters.get(i);
                paramsize += (v.array) ?  4 + v.arrayLength: 1;
                // System.out.println("\t" + v.name + "\t ... \t" + v.array + "\t ... \t" + v.arrayLength);
            }

            // System.out.println("- variables");
            for (int i = 0; i < f.allVariables.size(); i++) {
                Variable v = f.allVariables.get(i);
                paramsize += (v.array) ?  4 + v.arrayLength: 1;
                // System.out.println("\t" + v.name + "\t ... \t" + v.array + "\t ... \t" + v.arrayLength);
            }
        }

        // Generating code
        BrainfuckController.registerStart = paramsize + 1;
        BrainfuckController.stackBumper = BrainfuckController.registerStart + 20;
        BrainfuckController.stackTop  = BrainfuckController.stackBumper + 100;
        BrainfuckController.initStack();

        System.out.println("Generating code...");

        ParseTreeWalker walker = new ParseTreeWalker();
        CodeAnalysis analysis = new CodeAnalysis(lexer);
        MicroCListener listenerII = analysis;
        walker.walk(listenerII, tree);

        // Commit last function to the table
        // Remove last function from the flow
        // Append global code to the program
        // Append main to the program
        analysis.functionTable.put(analysis.activeFunction.name, analysis.activeFunction);
        FlowController.pop();

        BrainfuckController.append(analysis.functionTable.get("global").getActionCode(), 1);
        BrainfuckController.append(analysis.functionTable.get("main").getActionCode(), 1);

        String program = BrainfuckController.PROGRAM.toString();
        System.out.println(program);

        System.out.println("Output written to out.bf");


        try {
            File out = new File("out.bf");
            BufferedWriter bw = new BufferedWriter(new FileWriter(out));
            bw.write(program);
            bw.flush();
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}