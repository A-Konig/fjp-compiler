package CodeGenerating.Actions;

import CodeGenerating.Actions.Expressions.*;
import CodeGenerating.Actions.Expressions.Values.ArrayExpression;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.AssignmentType;
import Main.Variable;

/**
 * Represents and evaluates assignments
 *
 * @author kacerekz
 */
public class Assignment extends Action {

    /** Target variable */
    public Variable variable;

    /** Target array */
    public ArrayExpression arrayExpression;

    /** Determines type of assignment */
    public AssignmentType type;

    /** Value to assign */
    public Expression expression;

    /**
     * Generate code to execute this action
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        if (type == AssignmentType.EQUALS){
            // Execute, push, and append expression
            expression.executeAction(null);
            BrainfuckController.append(expression.getActionCode(), 1);

            // Zero out the original value
            BrainfuckController.append(">", variable.location);
            BrainfuckController.append("[-]", 1);
            BrainfuckController.append("<", variable.location);

            // Pop expression result to variable location
            BrainfuckController.pop(variable.location);

        } else {
            // Push variable
            BrainfuckController.push(variable.location);

            // Push expression
            expression.executeAction(null);
            BrainfuckController.append(expression.getActionCode(), 1);

            // On stack, right is on top
            BrainfuckController.pop(BrainfuckController.registerStart + 1);
            BrainfuckController.pop(BrainfuckController.registerStart + 0);

            // Do op
            switch (type){
                case TIMES_EQUALS:  BrainfuckController.mul(); break;
                case DIV_EQUALS:    BrainfuckController.div(); break;
                case MOD_EQUALS:    BrainfuckController.mod(); break;
                case PLUS_EQUALS:   BrainfuckController.add(); break;
                case MINUS_EQUALS:  BrainfuckController.sub(); break;
            }

            // Push result to variable location
            BrainfuckController.pop(variable.location);
        }
    }
}
