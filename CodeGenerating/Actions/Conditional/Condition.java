package CodeGenerating.Actions.Conditional;

import CodeGenerating.Actions.Action;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Types.ComparatorType;

/**
 * Describes and evaluates conditions
 *
 * @author kacerekz
 */
public class Condition extends Action {

    /** Left and right side of comparation to be evaluated */
    public Expression left, right;

    /** Determines comparation algorithm */
    public ComparatorType comparator = ComparatorType.EQUAL;

    /**
     * Evaluates left and right side of condition
     * Pops values to registers
     * Runs condition algorithm and pushes result to stack
     * Ends on memory:0
     * Generated code is in codeTarget
     *
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {

        // Two-sided condition
        if (right != null) {
            // Evaluate expressions and push them to the stack
            left.executeAction(null);
            right.executeAction(null);
            BrainfuckController.append(left.getActionCode(), 1);
            BrainfuckController.append(right.getActionCode(), 1);

            // Pop values in an order which reduces the number of BF comparison algorithms needed
            popValues();

            // Evaluate the condition and push its result to the stack
            evaluateCondition();

        // One-sided condition
        } else {
            left.executeAction(null);
            BrainfuckController.append(left.getActionCode(), 1);

            // Pop to reg:0
            // Normalize to 0 or 1, result is in reg:1
            // Push reg:1
            BrainfuckController.pop(BrainfuckController.registerStart);
            BrainfuckController.append(">", BrainfuckController.registerStart);
            BrainfuckController.append("[->[-]+<]", 1);
            BrainfuckController.append("<", BrainfuckController.registerStart);
            BrainfuckController.push(BrainfuckController.registerStart + 1);

        }

    }

    /**
     * Pops values depending on condition type
     * Flips greater than expressions so that they can be evaluated by less than algorithm
     */
    private void popValues() {
        switch (comparator){
            case EQUAL:
            case NOT_EQUAL:
            case LESS_THAN:
            case LESS_THAN_OR_EQUAL:
                BrainfuckController.pop(BrainfuckController.registerStart + 1);
                BrainfuckController.pop(BrainfuckController.registerStart);
                break;
            case GREATER_THAN:
            case GREATER_THAN_OR_EQUAL:
                BrainfuckController.pop(BrainfuckController.registerStart);
                BrainfuckController.pop(BrainfuckController.registerStart + 1);
                break;
        }
    }

    /**
     * Runs comparison algorithm and pushes result to stack
     */
    private void evaluateCondition() {
        // Adjust right side for equal condition (add 1)
        if (comparator == ComparatorType.LESS_THAN_OR_EQUAL
                || comparator == ComparatorType.GREATER_THAN_OR_EQUAL){
            BrainfuckController.append(">", BrainfuckController.registerStart + 1);
            BrainfuckController.append("+", 1);
            BrainfuckController.append("<", BrainfuckController.registerStart + 1);
        }

        // Now less/lequal and greater/grequal can all be calculated as less than
        // leaves three algorithms: equal, not equal, less than

        // run the comparison check
        switch (comparator){
            case EQUAL:
                BrainfuckController.append(">", BrainfuckController.registerStart);
                BrainfuckController.append("[->-<]+>[<->[-]]<", 1);
                break;

            case NOT_EQUAL:
                BrainfuckController.append(">", BrainfuckController.registerStart);
                BrainfuckController.append("[>>>+<<<-]>[>>-<+<-]>[<+>-]>[<<<+>>>[-]][-]<[-]<[-]<", 1);
                break;

            case LESS_THAN:
            case LESS_THAN_OR_EQUAL:
            case GREATER_THAN:
            case GREATER_THAN_OR_EQUAL:
                BrainfuckController.append(">", BrainfuckController.registerStart);
                BrainfuckController.append(">>>>[-]+>[-]<<<<[>+>+<<-]>[<+>-]<<[>>+<<-]+>>>[>-]>[<<<<->>[-]>>->]<+<<[>-[>-]>[<<<<->>[-]+>>->]<+<<-]>>[-]<[-]<[-]<[-]<", 1);
                break;
        }

        // Result is in register 0
        // Go to memory 0
        // Push register 0
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart);
        // Memory is at 0
    }

}
