package CodeGenerating.Controllers;

import Main.Variable;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages the scope naming and generates variable names
 *
 * @author kacerekz
 */
public class ScopeController {

    /** Current function name */
    public static  String functionName;

    /** Block structure */
    public static List<Integer> blockID;

    /** Block counter from function start */
    public static int blockCounter;

    /** Static initialization */
    static {
        blockID = new ArrayList<>();
        functionName = "";
        blockCounter = 0;
    }

    /**
     * Sets the function name and clears scope data
     * @param name Function name
     */
    public static void setFunctionName(String name){
        functionName = name;
        blockCounter = 0;
        blockID.clear();
    }

    /**
     * Adds a block to current scope
     */
    public static void enterBlock(){
        blockID.add(blockCounter);
        blockCounter++;
    }

    /**
     * Exits a block
     */
    public static void exitBlock(){
        blockID.remove(blockID.size() - 1);
    }

    /**
     * Generates a variable name as per current scope
     * @param identifier Variable name without scope data
     * @return Variable name with scope data (variable table key)
     */
    public static String getVariableName(String identifier){
        StringBuilder builder = new StringBuilder();

        builder.append(functionName);
        builder.append(".");

        for (int ID: blockID) {
            builder.append(ID);
            builder.append(".");
        }

        builder.append(identifier);

        return builder.toString();
    }

    /**
     * Multilevel variable search
     * Retrieves variables declared in this or a preceding scope
     *
     * @param identifier Variable name without scope data
     * @return Variable name with scope data
     */
    public static Variable getVariable(String identifier) {

        // Search for variable in this or higher level blocks
        for (int i = blockID.size(); i >= 0; i--){
            StringBuilder builder = new StringBuilder();

            builder.append(functionName);
            builder.append(".");

            for (int j = 0; j < i; j++) {
                int ID = blockID.get(j);
                builder.append(ID);
                builder.append(".");
            }

            builder.append(identifier);

            String key = builder.toString();
            if (DeclarationController.getVariable(key) != null){
                return DeclarationController.getVariable(key);
            }
        }

        // If no such variable is found in a preceding block, it must be a global variable
        String key = "global." + identifier;
        return DeclarationController.getVariable(key);
    }
}
