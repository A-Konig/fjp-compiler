package SemanticAnalysis;

import Grammar.MicroCLexer;
import Grammar.MicroCListener;
import Grammar.MicroCParser;
import Main.VariableType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 * Class going through created parse tree and checking for syntax analysis errors.
 */
public class SemanticListener implements MicroCListener {

    /** Lexer */
    private MicroCLexer lexer;
    /** Analyze helper */
    public AnalyzeHelper helper;

    /** Currently in expression */
    private boolean inExpression;

    /** Declaring function */
    private boolean funcDeclar;
    /** Declaring a parameter in a function */
    private boolean paramDeclar;
    /** Declaring variable */
    private boolean varDeclar;
    /** Operating with array variable */
    private boolean arrayExpression;
    /** In [] in array variable */
    private boolean arrayPos;

    /** Assigning to variable */
    private boolean varAssign;
    /** Calling a function */
    private boolean callinFunc;
    /** In condition */
    private boolean inConditionLine;

    /** Is current variable on the left side of expression an array */
    private boolean currIsArray;
    /** Was variable on the left side of expression already found */
    private boolean leftsideVarAssigned;

    /** Calling a function with parameter */
    private boolean paramCall;
    /** Declaring for cycle */
    private boolean forDeclr;

    /**
     * Constructor
     * @param lexer lexer
     */
    public SemanticListener(MicroCLexer lexer) {
        helper = new AnalyzeHelper();
        this.lexer = lexer;
    }

    // functions called when nodes visited

    @Override
    public void enterPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {    }

    @Override
    public void exitPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {    }

    @Override
    public void enterGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {    }

    @Override
    public void exitGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {    }

    @Override
    public void enterFunction(MicroCParser.FunctionContext ctx) {    }

    @Override
    public void exitFunction(MicroCParser.FunctionContext ctx) {    }

    /** Entering function declaration */
    @Override
    public void enterFuncHeader(MicroCParser.FuncHeaderContext ctx) {
        funcDeclar = true;
        helper.StartCodeSnippet(ctx);

    }

    /** Exiting function declaration */
    @Override
    public void exitFuncHeader(MicroCParser.FuncHeaderContext ctx) {
        funcDeclar = false;
        helper.EndCodeSnippet(ctx, "\t Mistake in function declaration: ");
    }

    @Override
    public void enterFuncName(MicroCParser.FuncNameContext ctx) {    }

    @Override
    public void exitFuncName(MicroCParser.FuncNameContext ctx) {    }

    /** Entering function declaration - parameter part */
    @Override
    public void enterFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
        paramDeclar = true;

    }

    /** Exiting function declaration - parameter part */
    @Override
    public void exitFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
        paramDeclar = false;

    }

    /** Entering function declaration - one parameter */
    @Override
    public void enterOneParamD(MicroCParser.OneParamDContext ctx) {
        leftsideVarAssigned = false;

    }

    /** Exiting function declaration - one parameter */
    @Override
    public void exitOneParamD(MicroCParser.OneParamDContext ctx) {
        leftsideVarAssigned = false;

    }

    /** Entering function call - parameters */
    @Override
    public void enterFuncParamCall(MicroCParser.FuncParamCallContext ctx) {
        paramCall = true;

    }

    /** Exiting function call - parameters */
    @Override
    public void exitFuncParamCall(MicroCParser.FuncParamCallContext ctx) {
        paramCall = false;

    }

    /** Entering function call - one parameter */
    @Override
    public void enterOneParamC(MicroCParser.OneParamCContext ctx) {
        leftsideVarAssigned = false;

    }

    /** Exiting function call - one parameter */
    @Override
    public void exitOneParamC(MicroCParser.OneParamCContext ctx) {
        leftsideVarAssigned = false;

    }

    @Override
    public void enterCode(MicroCParser.CodeContext ctx) {    }

    @Override
    public void exitCode(MicroCParser.CodeContext ctx) {    }

    /** Entering line of code */
    @Override
    public void enterLine(MicroCParser.LineContext ctx) {
        helper.StartCodeSnippet(ctx);

    }

    /** Exiting line of code */
    @Override
    public void exitLine(MicroCParser.LineContext ctx) {
        helper.EndCodeSnippet(ctx, "\t Mistake in line: ");

    }

    /** Entering variable declaration */
    @Override
    public void enterDeclaration(MicroCParser.DeclarationContext ctx) {
        varDeclar = true;
        currIsArray = false;
        leftsideVarAssigned = false;
    }

    /** Exiting variable declaration */
    @Override
    public void exitDeclaration(MicroCParser.DeclarationContext ctx) {
        varDeclar = false;
        currIsArray = false;

    }

    /** Entering array position */
    @Override
    public void enterArrayPos(MicroCParser.ArrayPosContext ctx) {
        arrayPos = true;

    }

    /** Exiting array position */
    @Override
    public void exitArrayPos(MicroCParser.ArrayPosContext ctx) {
        arrayPos = false;

    }

    /** Entering value in arrayPos */
    @Override
    public void enterValue(MicroCParser.ValueContext ctx) {
        arrayExpression = false;

    }

    @Override
    public void exitValue(MicroCParser.ValueContext ctx) {    }

    /** Entering assignment */
    @Override
    public void enterAssignment(MicroCParser.AssignmentContext ctx) {
        varAssign = true;
        currIsArray = false;
        leftsideVarAssigned = false;

    }

    /** Exiting assignment */
    @Override
    public void exitAssignment(MicroCParser.AssignmentContext ctx) {
        varAssign = false;
        currIsArray = false;
        leftsideVarAssigned = false;
    }

    @Override
    public void enterBlock(MicroCParser.BlockContext ctx) {     }

    @Override
    public void exitBlock(MicroCParser.BlockContext ctx) {    }

    /** Entering expression */
    @Override
    public void enterExpression(MicroCParser.ExpressionContext ctx) {
        inExpression = true;
        leftsideVarAssigned = false;

    }

    /** Exiting expression */
    @Override
    public void exitExpression(MicroCParser.ExpressionContext ctx) {
        inExpression = false;

    }

    @Override
    public void enterAExpression(MicroCParser.AExpressionContext ctx) {    }

    @Override
    public void exitAExpression(MicroCParser.AExpressionContext ctx) {    }

    @Override
    public void enterTExpression(MicroCParser.TExpressionContext ctx) {    }

    @Override
    public void exitTExpression(MicroCParser.TExpressionContext ctx) {    }

    @Override
    public void enterKExpression(MicroCParser.KExpressionContext ctx) {    }

    @Override
    public void exitKExpression(MicroCParser.KExpressionContext ctx) {    }

    @Override
    public void enterFExpression(MicroCParser.FExpressionContext ctx) {    }

    @Override
    public void exitFExpression(MicroCParser.FExpressionContext ctx) {    }

    @Override
    public void enterGExpression(MicroCParser.GExpressionContext ctx) {    }

    @Override
    public void exitGExpression(MicroCParser.GExpressionContext ctx) {    }

    /** Entering array expression */
    @Override
    public void enterArrayExpression(MicroCParser.ArrayExpressionContext ctx) {
        arrayExpression = true;
        arrayPos = false;

        helper.AddErrorToAnalyser("This version sadly does not support arrays in brainfuck code generation, sorry :(");
    }

    /** Exiting array expression */
    @Override
    public void exitArrayExpression(MicroCParser.ArrayExpressionContext ctx) {
        arrayExpression = false;

    }

    /** Entering function call */
    @Override
    public void enterFuncCall(MicroCParser.FuncCallContext ctx) {
        callinFunc = true;
        helper.StartFunctionCall();
    }

    /** Exiting function call */
    @Override
    public void exitFuncCall(MicroCParser.FuncCallContext ctx) {
        callinFunc = false;
        helper.EndFunctionCall();
    }

    /** Entering block */
    @Override
    public void enterIfBlock(MicroCParser.IfBlockContext ctx) {
        helper.StartBlock();

    }

    @Override
    public void exitIfBlock(MicroCParser.IfBlockContext ctx) {
        helper.EndBlock();

    }

    /** Entering condition line */
    @Override
    public void enterConditionLine(MicroCParser.ConditionLineContext ctx) {
        inConditionLine = true;
    }

    @Override
    public void exitConditionLine(MicroCParser.ConditionLineContext ctx) {
        inConditionLine = false;
    }

    @Override
    public void enterSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
        inConditionLine = true;
    }

    @Override
    public void exitSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
        inConditionLine = false;
    }

    /** Entering block */
    @Override
    public void enterForBlock(MicroCParser.ForBlockContext ctx) {
        helper.StartBlock();

    }

    @Override
    public void exitForBlock(MicroCParser.ForBlockContext ctx) {
        helper.EndBlock();

    }

    /** Entering for declaration */
    @Override
    public void enterForDeclaration(MicroCParser.ForDeclarationContext ctx) {
        forDeclr = true;
        helper.StartCodeSnippet(ctx);
    }

    @Override
    public void exitForDeclaration(MicroCParser.ForDeclarationContext ctx) {
        forDeclr = false;
        helper.EndCodeSnippet(ctx, "\t Mistake in for declaration: ");
    }

    /** Enter for variable assignment */
    @Override
    public void enterForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {
        varAssign = true;
    }

    @Override
    public void exitForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {
        varAssign = false;
    }

    @Override
    public void enterForVariableDecl(MicroCParser.ForVariableDeclContext ctx) { }

    @Override
    public void exitForVariableDecl(MicroCParser.ForVariableDeclContext ctx) { }

    /** Entering block */
    @Override
    public void enterWhileBlock(MicroCParser.WhileBlockContext ctx) {
        helper.StartBlock();

    }

    @Override
    public void exitWhileBlock(MicroCParser.WhileBlockContext ctx) {
        helper.EndBlock();

    }

    /** Entering block */
    @Override
    public void enterDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        helper.StartBlock();

    }

    @Override
    public void exitDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        helper.EndBlock();

    }

    @Override
    public void enterAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) { }

    @Override
    public void exitAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) { }

    @Override
    public void enterArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {    }

    @Override
    public void exitArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {    }

    @Override
    public void enterArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {    }

    @Override
    public void exitArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {    }

    @Override
    public void enterPostfixOperator(MicroCParser.PostfixOperatorContext ctx) { }

    @Override
    public void exitPostfixOperator(MicroCParser.PostfixOperatorContext ctx) { }

    @Override
    public void enterPrefixOperator(MicroCParser.PrefixOperatorContext ctx) {    }

    @Override
    public void exitPrefixOperator(MicroCParser.PrefixOperatorContext ctx){     }

    @Override
    public void enterComparator(MicroCParser.ComparatorContext ctx) { }

    @Override
    public void exitComparator(MicroCParser.ComparatorContext ctx) { }

    @Override
    public void enterType(MicroCParser.TypeContext ctx) { }

    @Override
    public void exitType(MicroCParser.TypeContext ctx) { }

    @Override
    public void enterFuncType(MicroCParser.FuncTypeContext ctx) { }

    @Override
    public void exitFuncType(MicroCParser.FuncTypeContext ctx) { }

    @Override
    public void enterFuncAccess(MicroCParser.FuncAccessContext ctx) { }

    @Override
    public void exitFuncAccess(MicroCParser.FuncAccessContext ctx) { }

    /** Entering terminal node */
    @Override
    public void visitTerminal(TerminalNode terminalNode) {
        // text of terminal node
        String nodeText = terminalNode.getText();

        // IDENTIFIER - found an identifier ( = string variable/function name)
        if (terminalNode.getSymbol().getType() == MicroCLexer.IDENTIFIER) {

            // declaring a new variable
            if (varDeclar && !inExpression) {

                // declaring new variable - identifier on the left side of the expression
                // - if left side already declared, declaring an array (found identifier in [])
                if (!leftsideVarAssigned) {
                    helper.DeclareVariable(nodeText, arrayExpression);
                    leftsideVarAssigned = true;
                }

                // declaring array length - in [] is identifier -> incorrect
                else {
                    helper.DeclaringArray(nodeText, false); //inChar
                    currIsArray = true;
                }

            }

            // assigning to a variable
            if (varAssign && !inExpression) {

                // variable thats an array on the left side of the expression
                if (arrayExpression && !leftsideVarAssigned) {
                    leftsideVarAssigned = true;
                    currIsArray = true;
                    helper.AssignToVariable(nodeText, true, true);
                }
                // variable that's not an array on the left side of the expression
                else if (!leftsideVarAssigned) {
                    helper.AssignToVariable(nodeText, currIsArray, true);
                    leftsideVarAssigned = true;
                }
                // in [] on the left side of the expression - does variable exist
                // if its an array arrayExpression is true
                else {
                    helper.CheckVariableExists(nodeText, arrayExpression);
                }
            }

            // on the right side of expression
            if (inExpression && !inConditionLine) {

                // check if not assigning to array w/o position specification
                helper.CheckVariableExists(currIsArray);

                // operating with array on the right side in expression
                if (arrayExpression) {
                    // does variable exist, is it an array
                    helper.AssignToVariable(nodeText, true, false);
                    helper.SetArrayName(nodeText);

                // non-array variable on the right side - check if variable exists
                } else {
                    helper.CheckVariableExists(nodeText, false);
                }

            }

            // declaring a new function
            if (funcDeclar) {

                // declaring parameters
                if (paramDeclar) {

                    // in [] - there cannot be an identifier (cannot declare variable length arrays)
                    if (leftsideVarAssigned) {
                        helper.FunctionParameterArray(nodeText, false);

                    // name of the parameter
                    } else {
                        helper.AddFunctionParameter(nodeText, arrayExpression);
                        leftsideVarAssigned = true;

                    }

                // declaring function name
                } else {
                    helper.DeclareFunction(nodeText);
                }

            }

            // calling a function
            if (callinFunc) {

                // name of called function
                if (!paramCall)
                    helper.CallFunction(nodeText);

                // call parameters
                else {
                    // in [] - indexing array
                    if (leftsideVarAssigned) {
                        helper.CheckVariableExists(nodeText, arrayExpression);

                    // name of parameter
                    } else {
                        helper.CallFunctionParameter(nodeText, arrayExpression, false);
                        leftsideVarAssigned = true;
                    }
                }
            }

            // testing in a condition
            if (inConditionLine) {

                // is array expression
                if (arrayExpression) {
                    helper.AssignToVariable(nodeText, true, false);
                    helper.SetArrayName(nodeText);

                // variable - does it exist
                } else
                    helper.CheckVariableInExpression(nodeText);

            }

            // for declaration line
            if (forDeclr) {
                // is array expression
                if (arrayExpression) {
                        helper.AssignToVariable(nodeText, true, false);
                        helper.SetArrayName(nodeText);

                // variable - does it exist
                } else
                    helper.CheckVariableInExpression(nodeText);
            }

        }

        // CHARACTER VALUE
        if (terminalNode.getSymbol().getType() == MicroCLexer.CHARVAL) {

            // declaring variable
            if (varDeclar && !inExpression) {

                // declaring array length - in [] either identifier or char
                if (arrayPos) {
                    currIsArray = true;
                }

            }

            // calling a function
            if (callinFunc) {
                // parameter is char
                if (!leftsideVarAssigned)
                    helper.CallFunctionParameter(nodeText, false, true);
            }

        }

        // declaring an INT
        if (terminalNode.getSymbol().getType() == MicroCLexer.INT) {
            if (varDeclar || paramDeclar)
                helper.SetType(VariableType.INTEGER);

        }

        // declaring a CHAR
        if (terminalNode.getSymbol().getType() == MicroCLexer.CHAR) {
            if (varDeclar || paramDeclar) {
                helper.SetType(VariableType.CHARACTER);
            }
        }

        // declaring a BOOL
        if (terminalNode.getSymbol().getType() == MicroCLexer.BOOL) {
            if (varDeclar || paramDeclar) {
                helper.SetType(VariableType.BOOL);
            }
        }

        // BOOLVAL - written true or false
        if (terminalNode.getSymbol().getType() == MicroCLexer.BOOLVAL) {
            if (callinFunc && paramCall) {
                if (!leftsideVarAssigned) {
                    helper.CallFunctionParameter(VariableType.BOOL);
                }
            }
        }

        // NUMBER - written constant in code
        if (terminalNode.getSymbol().getType() == MicroCLexer.NUMBER) {
            // assigning
            int number = Integer.parseInt(terminalNode.getText());

            // in function parameter
            if (callinFunc && paramCall) {
                // in []
                if (!leftsideVarAssigned) {
                    helper.CallFunctionParameter(VariableType.INTEGER);
                }
            }

            // declaring
            if (varDeclar && !inExpression) {
                // declaring an array - setting its length with a constant
                if (arrayPos) {
                    currIsArray = true;
                }
            }

            // assigning
            if (varAssign && !inExpression) {
                // in [] - accessing array position
                if (arrayPos) {
                    currIsArray = true;
                }
            }
        }
    }

    /** Enter error node */
    @Override
    public void visitErrorNode(ErrorNode errorNode) {
        helper.AddErrorToAnalyser(errorNode.getText());
    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {    }
}


/*
KONTROLA
- proměnné
    > deklarace před použitím
    > inicializace správnejma hodnotama
    > klíčem v symbol table je path
    > postup hledání block-current function-global
- pole
    > musí se deklarovat s určenou délkou
    > lze přiřazovat na index, počítat s indexem
    > nelze pracovat s proměnnou pole bez specifikování indexu
    - čím lze indexovat (viz typování)
- funkce
    > deklarace před voláním funkce
    > obsahuje Main
    > volané s dost/správnýma parametrama
- typování
    > co se mezi sebou smí převádět
- podmínky
    > co můžu testovat vůči čemu (viz typování) (můžu kontrolovat int == true bool == 36 atd)
- cykly
    > které proměnné můžou vest cyklus (viz typování)

SCHOVAM SI
- tabulka se proměnnýma - klíč path k proměnné
- tabulka s funkcema - klíč jméno funkce

NEKONTROLUJU
- pole
    - sahám na pozici co je mimo rozsah pole
    - deklaruju pole o délce 0
- cykly
    - potenciální "blbost" v deklaraci for (ex: for(int j = 2; i < 5; k++) )
*/