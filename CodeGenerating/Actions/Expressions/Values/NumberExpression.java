package CodeGenerating.Actions.Expressions.Values;

import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Represents a numeric expression
 *
 * @author kacerekz
 */
public class NumberExpression extends Expression {

    /** Integer value */
    public int value;

    /**
     * Create number expression of given value
     * @param value Integer value
     */
    public NumberExpression(int value){
        this.value = value;
    }

    /**
     * Pushes an integer value to the Brainfuck stack
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Go to reg:0
        // Put constant value
        // Push to stack from reg:0
        // End at mem:0
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("+", value);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart);
    }
}
