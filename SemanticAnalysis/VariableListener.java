package SemanticAnalysis;

import Grammar.MicroCLexer;
import Grammar.MicroCListener;
import Grammar.MicroCParser;
import Main.Variable;
import Main.VariableType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;

/**
 * Class that serves the purpose of creating function table containing functions and all their variables.
 *
 * Parsed code has to be already checked for syntactic errors.
 */
public class VariableListener implements MicroCListener {

    /** Lexer */
    MicroCLexer lexer;
    /** Variable counter */
    public VariableCounter counter;

    /** Currently in expression */
    private boolean inExpression;

    /** Declaring function */
    private boolean funcDeclar;
    /** Declaring a parameter in a function */
    private boolean paramDeclar;
    /** Declaring variable */
    private boolean varDeclar;
    /** In [] in array variable */
    private boolean arrayPos;
    /** Is current variable an array */
    private boolean currIsArray;

    /** Current function */
    private String currFunc;
    /** Current symbol (on the left side) */
    private String currSymbol;
    /** Current type */
    private VariableType currType;
    /** Length of array */
    private int arrLength;

    /** Constructor */
    public VariableListener(MicroCLexer lexer) {
        counter = new VariableCounter();
        this.lexer = lexer;
        currFunc = "";
    }

    @Override
    public void enterPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {

    }

    @Override
    public void exitPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {

    }

    @Override
    public void enterGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {

    }

    @Override
    public void exitGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {

    }

    @Override
    public void enterFunction(MicroCParser.FunctionContext ctx) {

    }

    @Override
    public void exitFunction(MicroCParser.FunctionContext ctx) {

    }

    @Override
    public void enterFuncHeader(MicroCParser.FuncHeaderContext ctx) {
        funcDeclar = true;

    }

    @Override
    public void exitFuncHeader(MicroCParser.FuncHeaderContext ctx) {
        funcDeclar = false;

    }

    @Override
    public void enterFuncName(MicroCParser.FuncNameContext ctx) {

    }

    @Override
    public void exitFuncName(MicroCParser.FuncNameContext ctx) {

    }

    @Override
    public void enterFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
        paramDeclar = true;

    }

    @Override
    public void exitFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
        paramDeclar = false;

    }

    @Override
    public void enterOneParamD(MicroCParser.OneParamDContext ctx) {

    }

    @Override
    public void exitOneParamD(MicroCParser.OneParamDContext ctx) {

    }

    @Override
    public void enterFuncParamCall(MicroCParser.FuncParamCallContext ctx) {

    }

    @Override
    public void exitFuncParamCall(MicroCParser.FuncParamCallContext ctx) {

    }

    @Override
    public void enterOneParamC(MicroCParser.OneParamCContext ctx) {

    }

    @Override
    public void exitOneParamC(MicroCParser.OneParamCContext ctx) {

    }

    @Override
    public void enterCode(MicroCParser.CodeContext ctx) {

    }

    @Override
    public void exitCode(MicroCParser.CodeContext ctx) {

    }

    @Override
    public void enterLine(MicroCParser.LineContext ctx) {

    }

    @Override
    public void exitLine(MicroCParser.LineContext ctx) {

    }

    @Override
    public void enterDeclaration(MicroCParser.DeclarationContext ctx) {
        varDeclar = true;
        currIsArray = false;

    }

    @Override
    public void exitDeclaration(MicroCParser.DeclarationContext ctx) {
        varDeclar = false;
        if (currIsArray)
            counter.DeclareArrayVar(currSymbol, currFunc, arrLength, currType);
        else
            counter.DeclareVar(currSymbol, currFunc, currType);
    }

    @Override
    public void enterArrayPos(MicroCParser.ArrayPosContext ctx) {
        arrayPos = true;

    }

    @Override
    public void exitArrayPos(MicroCParser.ArrayPosContext ctx) {
        arrayPos = false;

    }

    @Override
    public void enterValue(MicroCParser.ValueContext ctx) {

    }

    @Override
    public void exitValue(MicroCParser.ValueContext ctx) {

    }

    @Override
    public void enterAssignment(MicroCParser.AssignmentContext ctx) {
        currIsArray = false;

    }

    @Override
    public void exitAssignment(MicroCParser.AssignmentContext ctx) {    }

    @Override
    public void enterBlock(MicroCParser.BlockContext ctx) {
    }

    @Override
    public void exitBlock(MicroCParser.BlockContext ctx) {
    }

    @Override
    public void enterExpression(MicroCParser.ExpressionContext ctx) {
        inExpression = true;

    }

    @Override
    public void exitExpression(MicroCParser.ExpressionContext ctx) {
        inExpression = false;

    }

    @Override
    public void enterAExpression(MicroCParser.AExpressionContext ctx) {

    }

    @Override
    public void exitAExpression(MicroCParser.AExpressionContext ctx) {

    }

    @Override
    public void enterTExpression(MicroCParser.TExpressionContext ctx) {

    }

    @Override
    public void exitTExpression(MicroCParser.TExpressionContext ctx) {

    }

    @Override
    public void enterKExpression(MicroCParser.KExpressionContext ctx) {

    }

    @Override
    public void exitKExpression(MicroCParser.KExpressionContext ctx) {

    }

    @Override
    public void enterFExpression(MicroCParser.FExpressionContext ctx) {

    }

    @Override
    public void exitFExpression(MicroCParser.FExpressionContext ctx) {

    }

    @Override
    public void enterGExpression(MicroCParser.GExpressionContext ctx) {

    }

    @Override
    public void exitGExpression(MicroCParser.GExpressionContext ctx) {

    }

    @Override
    public void enterArrayExpression(MicroCParser.ArrayExpressionContext ctx) {

    }

    @Override
    public void exitArrayExpression(MicroCParser.ArrayExpressionContext ctx) {

    }

    @Override
    public void enterFuncCall(MicroCParser.FuncCallContext ctx) {

    }

    @Override
    public void exitFuncCall(MicroCParser.FuncCallContext ctx) {

    }

    @Override
    public void enterIfBlock(MicroCParser.IfBlockContext ctx) {
        counter.EnterNewBlock();
    }

    @Override
    public void exitIfBlock(MicroCParser.IfBlockContext ctx) {
        counter.ExitBlock();
    }

    @Override
    public void enterConditionLine(MicroCParser.ConditionLineContext ctx) {

    }

    @Override
    public void exitConditionLine(MicroCParser.ConditionLineContext ctx) {

    }

    @Override
    public void enterSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
    }

    @Override
    public void exitSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
    }

    @Override
    public void enterForBlock(MicroCParser.ForBlockContext ctx) {
        counter.EnterNewBlock();

    }

    @Override
    public void exitForBlock(MicroCParser.ForBlockContext ctx) {
        counter.ExitBlock();

    }

    @Override
    public void enterForDeclaration(MicroCParser.ForDeclarationContext ctx) {

    }

    @Override
    public void exitForDeclaration(MicroCParser.ForDeclarationContext ctx) {

    }

    @Override
    public void enterForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {

    }

    @Override
    public void exitForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {

    }

    @Override
    public void enterForVariableDecl(MicroCParser.ForVariableDeclContext ctx) {
    }

    @Override
    public void exitForVariableDecl(MicroCParser.ForVariableDeclContext ctx) {
    }

    @Override
    public void enterWhileBlock(MicroCParser.WhileBlockContext ctx) {
        counter.EnterNewBlock();

    }

    @Override
    public void exitWhileBlock(MicroCParser.WhileBlockContext ctx) {
        counter.ExitBlock();

    }

    @Override
    public void enterDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        counter.EnterNewBlock();

    }

    @Override
    public void exitDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        counter.ExitBlock();

    }

    @Override
    public void enterAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) {
    }

    @Override
    public void exitAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) {
    }

    @Override
    public void enterArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {

    }

    @Override
    public void exitArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {

    }

    @Override
    public void enterArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {

    }

    @Override
    public void exitArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {

    }

    @Override
    public void enterPostfixOperator(MicroCParser.PostfixOperatorContext ctx) {
    }

    @Override
    public void exitPostfixOperator(MicroCParser.PostfixOperatorContext ctx) {
    }

    @Override
    public void enterPrefixOperator(MicroCParser.PrefixOperatorContext ctx) {

    }

    @Override
    public void exitPrefixOperator(MicroCParser.PrefixOperatorContext ctx) {

    }

    @Override
    public void enterComparator(MicroCParser.ComparatorContext ctx) {
    }

    @Override
    public void exitComparator(MicroCParser.ComparatorContext ctx) {
    }

    @Override
    public void enterType(MicroCParser.TypeContext ctx) {
    }

    @Override
    public void exitType(MicroCParser.TypeContext ctx) {
    }

    @Override
    public void enterFuncType(MicroCParser.FuncTypeContext ctx) {
    }

    @Override
    public void exitFuncType(MicroCParser.FuncTypeContext ctx) {
    }

    @Override
    public void enterFuncAccess(MicroCParser.FuncAccessContext ctx) {
    }

    @Override
    public void exitFuncAccess(MicroCParser.FuncAccessContext ctx) {
    }

    /** Entering terminal node */
    @Override
    public void visitTerminal(TerminalNode terminalNode) {
        // IDENTIFIER - found an identifier
        if (terminalNode.getSymbol().getType() == MicroCLexer.IDENTIFIER) {

            // declaring a new variable
            if (varDeclar && !inExpression) {

                // declaring new variable - identifier on the left side of the expression
                if (!arrayPos) {
                    currSymbol = terminalNode.getText();
                }

                // declaring an array - in []
                else {
                    arrLength = (int)(terminalNode.getText().charAt(0));
                    currIsArray = true;
                }

            }

            // declaring a new function
            if (funcDeclar) {
                String varName = terminalNode.getText();

                // declaring parameters
                if (paramDeclar) {

                    // in []
                    if (arrayPos) {
                        counter.SetLastParamAsArray(currFunc, varName.charAt(0));

                    // name of parameter
                    } else {
                        counter.AddParameter(currFunc, varName, currType);
                    }

                // declaring function name
                } else {
                    currFunc = terminalNode.getText();
                    counter.DeclareFunc(terminalNode.getText());
                }

            }

        }

        // CHARACTER
        if (terminalNode.getSymbol().getType() == MicroCLexer.CHARVAL) {

            // declaring a new variable
            if (varDeclar && !inExpression) {

                // length specified by character
                if (arrayPos) {
                    arrLength = (int)(terminalNode.getText().charAt(1));
                    currIsArray = true;
                }

            }

            // declaring a new function
            if (funcDeclar) {
                String varName = terminalNode.getText();

                // declaring parameters - length given by char
                if (paramDeclar) {
                    if (arrayPos) {
                        counter.SetLastParamAsArray(currFunc, varName.charAt(1));
                    }
                }
            }
        }

        // declaring an INT
        if (terminalNode.getSymbol().getType() == MicroCLexer.INT) {
            if (varDeclar || paramDeclar)
                currType = VariableType.INTEGER;
        }

        // delcaring a CHAR
        if (terminalNode.getSymbol().getType() == MicroCLexer.CHAR) {
            if (varDeclar || paramDeclar)
                currType = VariableType.CHARACTER;
        }

        // declaring a BOOL
        if (terminalNode.getSymbol().getType() == MicroCLexer.BOOL) {
            if (varDeclar || paramDeclar)
                currType = VariableType.BOOL;
        }

        // NUMBER - written constant in code
        if (terminalNode.getSymbol().getType() == lexer.NUMBER) {
            // assigning
            int number = Integer.parseInt(terminalNode.getText());

            // declaring an array - setting its length with a constant
            if (varDeclar && !inExpression) {
                if (arrayPos) {
                    arrLength = number;
                    currIsArray = true;
                }
            }

            if (funcDeclar) {
                // declaring paramter array - setting its length with a constant
                if (paramDeclar)
                    if (arrayPos) {
                        counter.SetLastParamAsArray(currFunc, number);
                    }

            }
        }

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {
    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {
    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {
    }
}
