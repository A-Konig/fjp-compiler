package SemanticAnalysis;

import Main.Variable;
import Main.VariableType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

import java.util.ArrayList;

/**
 * Class used to call functions of Analysator class and operate with them to more accurately analyse code
 */
public class AnalyzeHelper {

    /** Analysator */
    public Analysator analysator;
    /** Currently processed function */
    private String currFunc;
    /** Currently processed symbol (on the left side of an expression) */
    private String currSymbol;
    /** Type of currently processed symbol */
    private VariableType currType;
    /** Calling function */
    private String funcCall;
    /** Calling fucntion with parameters */
    private ArrayList<Variable> funcParam;
    /** Name of currently processed array (on the right side of an expression) */
    private String arrayName;
    /** Code snippetStart */
    private int snippetStart;

    /** Constructor */
    AnalyzeHelper() {
        analysator = new Analysator();
        funcParam = new ArrayList<>();
        currFunc = "";
    }

    // saving text

    /**
     * Start saving currently processed snippetStart of code
     * @param ctx parse rule context
     */
    void StartCodeSnippet(ParserRuleContext ctx) {
        snippetStart = ctx.start.getStartIndex();

    }

    /**
     * End saving currently processed snippetStart of code.
     * If syntax error was found during processing add snippetStart to error list.
     *
     * @param ctx   parse rule context
     * @param s     additional potential error info
     */
    void EndCodeSnippet(ParserRuleContext ctx, String s) {
        int e = ctx.stop.getStopIndex();

        Interval interval = new Interval(this.snippetStart,e);
        String errorPos = ctx.start.getInputStream().getText(interval);

        if (analysator.foundErrorInLine)
            analysator.addPostError(s + errorPos);
    }

    // function

    /**
     * Start function call = create array list which will save function parameters
     */
    void StartFunctionCall() {
        funcParam = new ArrayList<>();
        
    }

    /**
     * End function call = check if function was called with correct parameters
     */
    void EndFunctionCall() {
        analysator.CheckFunctionCall(funcCall, currFunc, funcParam);

    }

    // block

    /**
     * Start block (while/for/if)
     */
    void StartBlock() {
        analysator.StartBlock();
    }

    /**
     * End block (while/for/if)
     */
    void EndBlock() {
        analysator.RollbackAfterBlock();

    }

    // reactions to terminal nodes

    /**
     * Declaring new variable
     * @param value     name of variable
     * @param isArray   is variable being declared in a block
     */
    void DeclareVariable(String value, boolean isArray) {
        currSymbol = value;

        // declaring new variable in a block
        // - test if variable can be declared in block, if yes add it into block variables that will be later deleted from symbol table
        boolean res = analysator.DeclareVar(currSymbol, currFunc, currType, isArray);

        if (analysator.blockLayer > 0) {
            if (res) analysator.AddVariableToBlock(currSymbol, currFunc);
        }
    }

    /**
     * Declaring array
     * - entered [x] part of array declaration, currently declared variable will be set to be an array of length x
     * - x can be either char literal or identifier, however identifier will not pass the syntax check
     * @param pos       length of array
     * @param inChar    is x a char literal
     */
    void DeclaringArray(String pos, boolean inChar) {
        if (!inChar)
            analysator.addError("Declaring arrays with variable length not allowed");

    }


    // assigning

    /**
     * using variable in assignment - check if exists
     * @param nodeText  variable that will be assigned to
     * @param isArray   can variable be an array
     * @param symbol    is variable on the left side of the assgnment
     */
    void AssignToVariable(String nodeText, boolean isArray, boolean symbol) {
        if (symbol)
            currSymbol = nodeText;

        analysator.AssigningVariable(nodeText, currFunc, isArray);
    }

    /**
     * Found array call on the right side of an equation - set arrayName
     * @param nodeText  identifier name
     */
    void SetArrayName(String nodeText) {
        arrayName = nodeText;
    }

    // check variables

    /**
     * Check if variable exits
     * @param currIsArray   can variable be an array or not
     */
    void CheckVariableExists(boolean currIsArray) {

        analysator.CheckExistenceVariable(currSymbol, currFunc, currIsArray);
    }

    /**
     * Check if variable can be in current expression
     * @param varName   name of variable
     */
    void CheckVariableInExpression(String varName) {

        analysator.CheckExistenceVariable(varName, currFunc, false);
    }

    /**
     * Check if variable exists
     * @param varName       name of variable
     * @param currIsArray   can variable be an array
     */
    void CheckVariableExists(String varName, boolean currIsArray) {
        analysator.CheckExistenceVariable(varName, currFunc, currIsArray);

    }

    // function declaration

    /**
     * Set last parameter of function as array, entered [x]
     * - x is either is a char literal or variable identifier
     * - variable identifier won't pass syntax analysis
     * @param varName   text of identifier
     * @param inChar    is parameter a char literal
     */
    void FunctionParameterArray(String varName, boolean inChar) {
        if (!inChar)
            analysator.addError("Declaring arrays with variable length not allowed");
    }

    /**
     * Add function parameter
     * @param varName   name of parameter
     */
    void AddFunctionParameter(String varName, boolean isArray) {
        analysator.AddParameter(currFunc, varName, currType, isArray);

    }

    /**
     * Declare a new function
     * @param nodeText  name of function
     */
    void DeclareFunction(String nodeText) {
        currFunc = nodeText;
        analysator.DeclareFunc(nodeText);
    }

    // call function

    /**
     * Call a function
     * @param nodeText  name of called function
     */
    void CallFunction(String nodeText) {
        funcCall = nodeText;

    }

    /**
     * Add call function parameter
     * - need to check if parameter exists
     * @param name      name of parameter
     * @param array    is given parameter indexed array
     * @param inChar    is given parameter a character literal
     */
    void CallFunctionParameter(String name, boolean array, boolean inChar) {

        // passing character literal as parameter
        if (inChar) {
            // create new variable
            Variable v = new Variable();
            v.type = VariableType.CHARACTER;
            funcParam.add(v);

        // identifier - does variable exist? is call valid?
        } else {
            Variable v = analysator.GetVariable(name, currFunc);
            if (v == null)
                analysator.addError("Assigning a non existing variable " + name + " as function parameter " + funcCall);
            else {
                // called as identifier
                if (!array) {
                    funcParam.add(v);
                }
                // called as indexed array
                else {
                    Variable param = new Variable();
                    param.name = v.name;
                    param.array = !array;
                    funcParam.add(param);
                }

            }
        }
    }

    /**
     * Add call function parameter
     * - need to check if parameter exists
     * @param type  type of parameter
     */
    void CallFunctionParameter(VariableType type) {
        Variable v = new Variable();
        v.type = type;
        funcParam.add(v);
    }

    /**
     * Set current type
     * @param type  type
     */
    void SetType(VariableType type) {
        currType = type;
    }

    /**
     * Set position of array - entered [x] where x is integer value
     * - either setting currentSymbol (= [x] is on the left side of equation)
     * - or setting arrayName (= [x] is on the right side of equation)
     * @param number    array position
     * @param current   if true setting currentSymbol, if false setting arrayName
     */
    void SetArrayPos(int number, boolean current) {
        if (current)
            analysator.SetArrayPos(currSymbol, currFunc, number);
        else
            analysator.SetArrayPos(arrayName, currFunc, number);

    }

    /**
     * Add error to error list
     * @param text  error text
     */
    void AddErrorToAnalyser(String text) {
        analysator.addError(text);

    }

}