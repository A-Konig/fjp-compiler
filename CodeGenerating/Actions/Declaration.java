package CodeGenerating.Actions;

import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.*;
import Main.VariableType;

/**
 * Represents a variable declaration
 *
 * @author kacerekz
 */
public class Declaration extends Action {

    /** Variable type */
    public VariableType type;

    /** Variable name without block names */
    public String variableName;

    /** Array size. Negative for non-arrays, zero while waiting for size to be assigned. */
    public int arraySize;

    /** Expression to evaluate at first assignment */
    public Expression expression;

    /**
     * Initialize values
     */
    public Declaration(){
        variableName = null;
        arraySize = -1;
        expression = null;
    }

    /**
     * Sets the type of the variable
     * @param terminalText Variable type
     */
    public void setType(String terminalText){
        switch (terminalText){
            case "int":
            case "integer":
                type = VariableType.INTEGER;
                break;
            case "bool":
            case "_Bool":
                type = VariableType.BOOL;
                break;
            case "char":
                type = VariableType.CHARACTER;
                break;
        }
    }

    /**
     * Executes the declaration
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode){
        // Creates a variable in the variable table from declaration data
        // Retrieves the location to which an expression result could be evaluated
        int location = DeclarationController.setVariable(this);

        // If there is no expression to evaluate, we're done - we only assigned memory
        // If there's and expression, evaluate it - the result will be on the stack
        // Pop from stack to the location
        if (expression != null){

            // ENTER - Memory expected to be at 0

            // Put expression result on the stack
            expression.executeAction(null);
            BrainfuckController.append(expression.getActionCode(), 1);

            // Zero out variable location
            BrainfuckController.append(">", location);
            BrainfuckController.append("[-]", location);
            BrainfuckController.append("<", location);

            // Pop from stack to location
            BrainfuckController.pop(location);

            // EXIT - Memory expected to be at 0
        }
    }

    /**
     * Debug log format
     * @return The declaration as a string
     */
    @Override
    public String toString() {
        return "Declaration{" +
                "type=" + type +
                ", variableName='" + ScopeController.getVariableName(variableName) + '\'' +
                ", arraySize=" + arraySize +
                ", expression=" + expression +
                ", location=" + DeclarationController.getVariableLocation(variableName) +
                '}';
    }

}
