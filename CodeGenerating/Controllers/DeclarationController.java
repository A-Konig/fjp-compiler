package CodeGenerating.Controllers;

import CodeGenerating.Actions.Declaration;
import CodeGenerating.Actions.Function;
import Main.Variable;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

/**
 * Manages declarations, variable properties and the variable table
 *
 * @author kacerekz
 */
public class DeclarationController {

    /** Variable table */
    private static Hashtable<String, Variable> variableTable;

    /** Number of variables / assigned memory counter */
    private static int variableCounter;

    /** Static initialization */
    static {
        variableTable = new Hashtable<>();
        variableCounter = 0;
    }

    /**
     * Returns variable location in the memory
     * @param identifier Variable name without scope data
     * @return Variable location in the memory
     */
    public static int getVariableLocation(String identifier){
        String key = ScopeController.getVariableName(identifier);
        return variableTable.get(key).location;
    }

    /**
     * Adds a variable to the variable table
     * @param declaration Declaration data
     * @return Sets variable properties as per the declaration
     */
    public static int setVariable(Declaration declaration) {
        String key = ScopeController.getVariableName(declaration.variableName);

        Variable var = new Variable();
        var.functionName = key;
        var.name = declaration.variableName;
        var.type = declaration.type;
        var.array = declaration.arraySize > 0;
        var.arrayLength = declaration.arraySize;
        var.location = variableCounter;

        variableTable.put(key, var);

        if (declaration.arraySize > 0){
            // Only move by one cell
            variableCounter += 4 + declaration.arraySize;
        } else {
            // Move by indexing cells + array cells + one cell
            variableCounter += 1;
        }

        return var.location;
    }

    /**
     * Returns a variable
     * @param key Variable name with scope data
     * @return Variable
     */
    public static Variable getVariable(String key) {
        return variableTable.get(key);
    }

    /**
     * Returns all variables in a given function
     * @param function Function
     * @return Variable set of the function
     */
    public static List<Variable> getAllVariables(Function function) {
        Set<String> keySet = variableTable.keySet();
        List<Variable> variables = new ArrayList<>();

        for (String key : keySet){
            if (key.startsWith(function.name)){
                variables.add(variableTable.get(key));
            }
        }

        return variables;
    }
}
