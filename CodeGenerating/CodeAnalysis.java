package CodeGenerating;

import CodeGenerating.Actions.*;
import CodeGenerating.Actions.Conditional.*;
import CodeGenerating.Actions.Expressions.*;
import CodeGenerating.Actions.Expressions.Operations.*;
import CodeGenerating.Actions.Expressions.Values.*;
import CodeGenerating.Controllers.*;
import CodeGenerating.Types.*;
import Grammar.MicroCLexer;
import Grammar.MicroCListener;
import Grammar.MicroCParser;
import Main.Variable;
import Main.VariableType;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashMap;

/**
 * Code analysis and generating
 *
 * @author kacerekz
 */
public class CodeAnalysis implements MicroCListener {

    /** Lexer */
    MicroCLexer lexer;

    /** Active declaration */
    public Declaration activeDeclaration;

    /** Active assignment */
    public Assignment activeAssignment;

    /** Active expression */
    public Expression activeExpression;

    /** To be captured by a PostfixOP */
    public Variable activeVariable;

    /** To capture a Variable */
    public PrefixOP activePrefix;


    /** Current function being processed */
    public Function activeFunction;

    /** Table of processed functions */
    public HashMap<String, Function> functionTable;

    /** Function being called */
    public Function calledFunction;

    /** Function call code */
    public StringBuilder functionCall;

    /** Function call parameter counter */
    public int parameterCounter;


    public Condition activeCondition;

    public IfBlock activeIf;

    public ForBlock activeFor;

    public ForStartAssignment forStart;

    public ForEndAssignment forEnd;

    public WhileBlock activeWhile;

    public DoWhileBlock activeDoWhile;


    public ArrayExpression activeArray;


    /** Last visited terminal text */
    public String terminalText;

    /** Type of last visited terminal */
    public int terminalType;



    public CodeAnalysis(MicroCLexer lexer) {
        this.lexer = lexer;
        functionTable = new HashMap<>();
        activeFunction = new Function();
        activeFunction.setName("global");
        ScopeController.setFunctionName(activeFunction.name);
        FlowController.push(activeFunction);

        Declaration d = new Declaration();
        d.type = VariableType.INTEGER;
        d.variableName = "reg0";
        d.arraySize = -1;
        d.executeAction(null);
        BrainfuckController.append(d.getActionCode(), 1);

        Function printf = new Function();
        printf.name = "printf";
        printf.parameters.add(ScopeController.getVariable("reg0"));

        Function getchar = new Function();
        getchar.name = "getchar";
        getchar.parameters.add(ScopeController.getVariable("reg0"));

        functionTable.put("printf", printf);
        functionTable.put("getchar", getchar);
    }

    @Override
    public void enterPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {
    }

    @Override
    public void exitPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx) {
    }

    @Override
    public void enterGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {
    }

    @Override
    public void exitGlobalVariables(MicroCParser.GlobalVariablesContext ctx) {
    }

    @Override
    public void enterFunction(MicroCParser.FunctionContext ctx) {
        // Remove old function from the flow
        // Put old function code in the table
        // Activate new function
        // Add it to the flow
        FlowController.pop();
        functionTable.put(activeFunction.name, activeFunction);
        activeFunction = new Function();
        FlowController.push(activeFunction);
    }

    @Override
    public void exitFunction(MicroCParser.FunctionContext ctx) {
    }

    @Override
    public void enterFuncHeader(MicroCParser.FuncHeaderContext ctx) {
    }

    @Override
    public void exitFuncHeader(MicroCParser.FuncHeaderContext ctx) {
    }

    @Override
    public void enterFuncName(MicroCParser.FuncNameContext ctx) {
    }

    @Override
    public void exitFuncName(MicroCParser.FuncNameContext ctx) {
        activeFunction.setName(terminalText);
        ScopeController.setFunctionName(activeFunction.name);
    }

    @Override
    public void enterFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
    }

    @Override
    public void exitFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx) {
    }

    @Override
    public void enterOneParamD(MicroCParser.OneParamDContext ctx) {
        activeDeclaration = new Declaration();
    }

    @Override
    public void exitOneParamD(MicroCParser.OneParamDContext ctx) {
        // Declare variable
        // Add it to the active function's variable list
        activeDeclaration.executeAction(null);
        activeFunction.parameters.add(ScopeController.getVariable(activeDeclaration.variableName));
        activeDeclaration = null;
    }

    @Override
    public void enterFuncParamCall(MicroCParser.FuncParamCallContext ctx) {
    }

    @Override
    public void exitFuncParamCall(MicroCParser.FuncParamCallContext ctx) {
    }

    @Override
    public void enterOneParamC(MicroCParser.OneParamCContext ctx) {
    }

    @Override
    public void exitOneParamC(MicroCParser.OneParamCContext ctx) {
        // Set the codeTarget
        // Find variable to set
        StringBuilder oldTarget = BrainfuckController.codeTarget;
        BrainfuckController.codeTarget = functionCall;
        Variable toSet = calledFunction.parameters.get(parameterCounter);

        if (activeArray == null) {
            // Set parameter from last visited terminal data
            switch (terminalType){
                case MicroCLexer.IDENTIFIER:
                    Variable id = ScopeController.getVariable(terminalText);
                    BrainfuckController.copy(id.location, toSet.location, BrainfuckController.registerStart);
                    calledFunction.toRead = id;
                    break;

                case MicroCLexer.NUMBER:
                    BrainfuckController.append(">", toSet.location);
                    BrainfuckController.append("+", Integer.parseInt(terminalText));
                    BrainfuckController.append("<", toSet.location);
                    break;

                case MicroCLexer.CHARVAL:
                    BrainfuckController.append(">", toSet.location);
                    BrainfuckController.append("+", terminalText.charAt(1));
                    BrainfuckController.append("<", toSet.location);
                    break;

                case MicroCLexer.BOOLVAL:
                    BrainfuckController.append(">", toSet.location);
                    BrainfuckController.append("+", terminalText.equals("true") ? 1 : 0);
                    BrainfuckController.append("<", toSet.location);
                    break;
            }
        } else {
            // Execute array expression
            // Pop result from the stack to location
            activeArray.executeAction(null);
            BrainfuckController.append(activeArray.getActionCode(), 1);
            BrainfuckController.pop(toSet.location);
        }

        // Reset target
        BrainfuckController.codeTarget = oldTarget;

        // Increase parameter count
        parameterCounter++;
    }

    @Override
    public void enterCode(MicroCParser.CodeContext ctx) {
    }

    @Override
    public void exitCode(MicroCParser.CodeContext ctx) {
    }

    @Override
    public void enterLine(MicroCParser.LineContext ctx) {
    }

    @Override
    public void exitLine(MicroCParser.LineContext ctx) {
    }

    @Override
    public void enterDeclaration(MicroCParser.DeclarationContext ctx) {
        activeDeclaration = new Declaration();
    }

    @Override
    public void exitDeclaration(MicroCParser.DeclarationContext ctx) {
        // Capture expression, if any
        if (activeExpression != null){
            activeDeclaration.expression = activeExpression;
            activeExpression = null;
        }

        // Execute declaration
        activeDeclaration.executeAction(null);
        BrainfuckController.append(activeDeclaration.getActionCode(), 1);
        activeDeclaration = null;
    }

    @Override
    public void enterArrayPos(MicroCParser.ArrayPosContext ctx) {
    }

    @Override
    public void exitArrayPos(MicroCParser.ArrayPosContext ctx) {
    }

    @Override
    public void enterValue(MicroCParser.ValueContext ctx) {
    }

    @Override
    public void exitValue(MicroCParser.ValueContext ctx) {
    }

    @Override
    public void enterAssignment(MicroCParser.AssignmentContext ctx) {
        activeAssignment = new Assignment();
    }

    @Override
    public void exitAssignment(MicroCParser.AssignmentContext ctx) {
        // Capture expression
        activeAssignment.expression = activeExpression;
        activeExpression = null;

        // Execute assignment
        activeAssignment.executeAction(null);
        BrainfuckController.append(activeAssignment.getActionCode(), 1);
        activeAssignment = null;
    }

    @Override
    public void enterBlock(MicroCParser.BlockContext ctx) {
        ScopeController.enterBlock();
    }

    @Override
    public void exitBlock(MicroCParser.BlockContext ctx) {
        ScopeController.exitBlock();
    }

    @Override
    public void enterExpression(MicroCParser.ExpressionContext ctx) {
    }

    @Override
    public void exitExpression(MicroCParser.ExpressionContext ctx) {
        // Expression must be consumed ( = set to null) and executed by its user

        // Condition consumes the expression
        if (activeCondition != null){

            // Set left expression
            if (activeCondition.left == null)  {
                activeCondition.left = activeExpression;

            // Set right expression
            } else if (activeCondition.right == null) {
                activeCondition.right = activeExpression;
            }

            // Consume expression
            activeExpression = null;
        }

        // For block consumes expression
        if (forStart != null){
            forStart.expression = activeExpression;
            activeExpression = null;
        }

        if (forEnd != null){
            forEnd.expression = activeExpression;
            activeExpression = null;
        }
    }

    @Override
    public void enterAExpression(MicroCParser.AExpressionContext ctx) {
        ExpressionA e = new ExpressionA();

        if (activeExpression != null) {
            activeExpression.setExpressionA(e);
            e.parent = activeExpression;
        }

        activeExpression = e;
    }

    @Override
    public void exitAExpression(MicroCParser.AExpressionContext ctx) {
        // If this is a top level expression (an AExpression)
        // Leave this node to be evaluated in the Expression node
        // Otherwise reset to parent
        activeExpression = (activeExpression.parent == null) ? activeExpression : activeExpression.parent;
    }

    @Override
    public void enterTExpression(MicroCParser.TExpressionContext ctx) {
        ExpressionT e = new ExpressionT();
        activeExpression.setExpressionT(e);
        e.parent = activeExpression;
        activeExpression = e;
    }

    @Override
    public void exitTExpression(MicroCParser.TExpressionContext ctx) {
        activeExpression = activeExpression.parent;
    }

    @Override
    public void enterKExpression(MicroCParser.KExpressionContext ctx) {
        ExpressionK e = new ExpressionK();
        activeExpression.setExpressionK(e);
        e.parent = activeExpression;
        activeExpression = e;
    }

    @Override
    public void exitKExpression(MicroCParser.KExpressionContext ctx) {
        activeExpression = activeExpression.parent;
    }

    @Override
    public void enterFExpression(MicroCParser.FExpressionContext ctx) {
        ExpressionF e = new ExpressionF();
        activeExpression.setExpressionF(e);
        e.parent = activeExpression;
        activeExpression = e;
    }

    @Override
    public void exitFExpression(MicroCParser.FExpressionContext ctx) {
        activeExpression = activeExpression.parent;
    }

    @Override
    public void enterGExpression(MicroCParser.GExpressionContext ctx) {
        ExpressionG e = new ExpressionG();
        activeExpression.setExpressionG(e);
        e.parent = activeExpression;
        activeExpression = e;
    }

    @Override
    public void exitGExpression(MicroCParser.GExpressionContext ctx) {
        activeExpression = activeExpression.parent;
    }

    @Override
    public void enterArrayExpression(MicroCParser.ArrayExpressionContext ctx) {
        // Mark current declaration as an array by setting array size to 0
        if (activeDeclaration != null
                && activeExpression == null
                && activeDeclaration.arraySize < 0) {
            activeDeclaration.arraySize = 0;
        }
    }

    @Override
    public void exitArrayExpression(MicroCParser.ArrayExpressionContext ctx) {
    }

    @Override
    public void enterFuncCall(MicroCParser.FuncCallContext ctx) {
        // Parse the function call to get the identifier
        String text = ctx.getText();
        String funcName = text.substring(0, text.indexOf('('));
        calledFunction = functionTable.get(funcName);
        functionCall = new StringBuilder();
        parameterCounter = 0;
    }

    @Override
    public void exitFuncCall(MicroCParser.FuncCallContext ctx) {
        // Execute function directly
        calledFunction.executeDirectAction(functionCall.toString());
    }

    @Override
    public void enterIfBlock(MicroCParser.IfBlockContext ctx) {
        // Create the if block
        // Redirect code to it
        // But then redirect code to nestedCollector
        activeIf = new IfBlock();
        FlowController.push(activeIf);
    }

    @Override
    public void exitIfBlock(MicroCParser.IfBlockContext ctx) {
        // Redirect to parent
        FlowController.pop();

        // Save code generated in the block
        StringBuilder nestedCode = activeIf.codeTarget;
        activeIf.codeTarget = new StringBuilder();

        // Write to block again
        activeIf.executeAction(nestedCode.toString());
        BrainfuckController.append(activeIf.getActionCode(), 1);

        // Dispose of block
        activeIf = null;
    }

    @Override
    public void enterConditionLine(MicroCParser.ConditionLineContext ctx) {
    }

    @Override
    public void exitConditionLine(MicroCParser.ConditionLineContext ctx) {
     }

    @Override
    public void enterSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
        // Activate a condition
        activeCondition = new Condition();
        if (activeDoWhile    != null) activeDoWhile.condition = activeCondition;
        else if (activeFor   != null) activeFor.condition = activeCondition;
        else if (activeIf    != null) activeIf.condition = activeCondition;
        else if (activeWhile != null) activeWhile.condition = activeCondition;
    }

    @Override
    public void exitSimpleCondition(MicroCParser.SimpleConditionContext ctx) {
        activeCondition = null;
    }

    @Override
    public void enterForBlock(MicroCParser.ForBlockContext ctx) {
        activeFor = new ForBlock();
        FlowController.push(activeFor);
    }

    @Override
    public void exitForBlock(MicroCParser.ForBlockContext ctx) {
        // Redirect to parent
        FlowController.pop();

        // Save code generated in the block
        StringBuilder nestedCode = activeFor.codeTarget;
        activeFor.codeTarget = new StringBuilder();

        // Write to block again
        activeFor.executeAction(nestedCode.toString());
        BrainfuckController.append(activeFor.getActionCode(), 1);

        // Dispose of block
        activeFor = null;
    }

    @Override
    public void enterForDeclaration(MicroCParser.ForDeclarationContext ctx) {
    }

    @Override
    public void exitForDeclaration(MicroCParser.ForDeclarationContext ctx) {
    }

    @Override
    public void enterForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {
        forEnd = new ForEndAssignment();
    }

    @Override
    public void exitForVariableAssign(MicroCParser.ForVariableAssignContext ctx) {
        activeFor.endAssignment = forEnd;
        forEnd = null;
    }

    @Override
    public void enterForVariableDecl(MicroCParser.ForVariableDeclContext ctx) {
        forStart = new ForStartAssignment();
        forStart.type = AssignmentType.EQUALS;
    }

    @Override
    public void exitForVariableDecl(MicroCParser.ForVariableDeclContext ctx) {
        activeFor.startAssignment = forStart;
        forStart = null;
    }

    @Override
    public void enterWhileBlock(MicroCParser.WhileBlockContext ctx) {
        activeWhile = new WhileBlock();
        FlowController.push(activeWhile);
    }

    @Override
    public void exitWhileBlock(MicroCParser.WhileBlockContext ctx) {
        // Redirect to parent
        FlowController.pop();

        // Save code generated in the block
        StringBuilder nestedCode = activeWhile.codeTarget;
        activeWhile.codeTarget = new StringBuilder();

        // Write to block again
        activeWhile.executeAction(nestedCode.toString());
        BrainfuckController.append(activeWhile.getActionCode(), 1);

        // Dispose of block
        activeWhile = null;
    }

    @Override
    public void enterDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        activeDoWhile = new DoWhileBlock();
        FlowController.push(activeDoWhile);
    }

    @Override
    public void exitDoWhileBlock(MicroCParser.DoWhileBlockContext ctx) {
        // Redirect to parent
        FlowController.pop();

        // Save code generated in the block
        StringBuilder nestedCode = activeDoWhile.codeTarget;
        activeDoWhile.codeTarget = new StringBuilder();

        // Write to block again
        activeDoWhile.executeAction(nestedCode.toString());
        BrainfuckController.append(activeDoWhile.getActionCode(), 1);

        // Dispose of block
        activeDoWhile = null;
    }

    @Override
    public void enterAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) {
        if (forEnd == null){
            switch (ctx.getText()) {
                case "="  : activeAssignment.type = AssignmentType.EQUALS; break;
                case "*=" : activeAssignment.type = AssignmentType.TIMES_EQUALS; break;
                case "/=" : activeAssignment.type = AssignmentType.DIV_EQUALS; break;
                case "%=" : activeAssignment.type = AssignmentType.MOD_EQUALS; break;
                case "+=" : activeAssignment.type = AssignmentType.PLUS_EQUALS; break;
                case "-=" : activeAssignment.type = AssignmentType.MINUS_EQUALS; break;
            }

        } else {
            switch (ctx.getText()) {
                case "="  : forEnd.type = AssignmentType.EQUALS; break;
                case "*=" : forEnd.type = AssignmentType.TIMES_EQUALS; break;
                case "/=" : forEnd.type = AssignmentType.DIV_EQUALS; break;
                case "%=" : forEnd.type = AssignmentType.MOD_EQUALS; break;
                case "+=" : forEnd.type = AssignmentType.PLUS_EQUALS; break;
                case "-=" : forEnd.type = AssignmentType.MINUS_EQUALS; break;
            }
        }
    }

    @Override
    public void exitAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx) {
    }

    @Override
    public void enterArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {
    }

    @Override
    public void exitArithmOpL1(MicroCParser.ArithmOpL1Context ctx) {
        switch (terminalText){
            case "+": activeExpression.setArithmeticOP1(new ArithmeticOP1(OPTypeArithmetic1.OP_ADD)); break;
            case "-": activeExpression.setArithmeticOP1(new ArithmeticOP1(OPTypeArithmetic1.OP_SUB)); break;
            default: System.err.println("Parsing unknown arithmetic operation at level 1.");
        }
    }

    @Override
    public void enterArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {
    }

    @Override
    public void exitArithmOpL2(MicroCParser.ArithmOpL2Context ctx) {
        switch (terminalText){
            case "*": activeExpression.setArithmeticOP2(new ArithmeticOP2(OPTypeArithmetic2.OP_MUL)); break;
            case "/": activeExpression.setArithmeticOP2(new ArithmeticOP2(OPTypeArithmetic2.OP_DIV)); break;
            case "%": activeExpression.setArithmeticOP2(new ArithmeticOP2(OPTypeArithmetic2.OP_MOD)); break;
            default: System.err.println("Parsing unknown arithmetic operation at level 2.");
        }
    }

    @Override
    public void enterPostfixOperator(MicroCParser.PostfixOperatorContext ctx) {
    }

    @Override
    public void exitPostfixOperator(MicroCParser.PostfixOperatorContext ctx) {
        PostfixOP op = null;

        switch (terminalText){
            case "++": op = new PostfixOP(OPTypePostfix.OP_ADD); break;
            case "--": op = new PostfixOP(OPTypePostfix.OP_SUB); break;
            default: System.err.println("Parsing unknown postfix operator.");
        }

        activeExpression.setPostfixOP(op);
        op.setVariable(activeVariable);
    }

    @Override
    public void enterPrefixOperator(MicroCParser.PrefixOperatorContext ctx) {
    }

    @Override
    public void exitPrefixOperator(MicroCParser.PrefixOperatorContext ctx) {
        PrefixOP op = null;

        switch (terminalText){
            case "++": op = new PrefixOP(OPTypePrefix.OP_ADD); break;
            case "--": op = new PrefixOP(OPTypePrefix.OP_SUB); break;
            default: System.err.println("Parsing unknown prefix operator.");
        }

        activeExpression.setPrefixOP(op);
        activePrefix = op;
    }

    @Override
    public void enterComparator(MicroCParser.ComparatorContext ctx) {
        switch (ctx.getText()) {
            case "==" : activeCondition.comparator = ComparatorType.EQUAL; break;
            case ">=" : activeCondition.comparator = ComparatorType.GREATER_THAN_OR_EQUAL; break;
            case "<=" : activeCondition.comparator = ComparatorType.LESS_THAN_OR_EQUAL; break;
            case "!=" : activeCondition.comparator = ComparatorType.NOT_EQUAL; break;
            case ">"  : activeCondition.comparator = ComparatorType.GREATER_THAN; break;
            case "<"  : activeCondition.comparator = ComparatorType.LESS_THAN; break;
        }
    }

    @Override
    public void exitComparator(MicroCParser.ComparatorContext ctx) {
    }

    @Override
    public void enterType(MicroCParser.TypeContext ctx) {
    }

    @Override
    public void exitType(MicroCParser.TypeContext ctx) {
        if (activeDeclaration != null){
            activeDeclaration.setType(terminalText);
        }
    }

    @Override
    public void enterFuncType(MicroCParser.FuncTypeContext ctx) {
    }

    @Override
    public void exitFuncType(MicroCParser.FuncTypeContext ctx) {
    }

    @Override
    public void enterFuncAccess(MicroCParser.FuncAccessContext ctx) {
    }

    @Override
    public void exitFuncAccess(MicroCParser.FuncAccessContext ctx) {
    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {
        boolean done = false;

        // Save terminal
        terminalText = terminalNode.getText();
        terminalType = terminalNode.getSymbol().getType();

        // ForBlock start
        if (forStart != null && terminalType == MicroCLexer.IDENTIFIER){
            if (activeArray == null){
                forStart.variable = ScopeController.getVariable(terminalText);
            } else {
                forStart.arrayExpression = activeArray;
                forStart.arrayExpression.variable = ScopeController.getVariable(terminalText);
            }
            done = true;
        }

        if (done) return;


        // ForBlock end
        if (forEnd != null && terminalType == MicroCLexer.IDENTIFIER){
            if (activeArray == null){
                forEnd.variable = ScopeController.getVariable(terminalText);
            } else {
                forEnd.arrayExpression = activeArray;
                forEnd.arrayExpression.variable = ScopeController.getVariable(terminalText);
            }
            done = true;
        }

        if (done) return;


        // Capture terminals during declaration
        if (activeDeclaration != null){
            // Capture:
            // - declaration name
            switch (terminalType){
                // Capture declared variable name
                case MicroCLexer.IDENTIFIER:
                    if (activeDeclaration.variableName == null){
                        activeDeclaration.variableName = terminalText;
                    }
                    break;

                // Capture array length if an array is being declared
                case MicroCLexer.NUMBER:
                    if (activeDeclaration.arraySize == 0){
                        activeDeclaration.arraySize = Integer.parseInt(terminalText);
                    }

                // Capture array length if an array is being declared
                case MicroCLexer.CHARVAL:
                    if (activeDeclaration.arraySize == 0){
                        // Character value format 'x'
                        activeDeclaration.arraySize = terminalText.charAt(1);
                    }
            }
        }

        // Capture values in expression
        if (activeExpression != null){
            switch (terminalType){
                case MicroCLexer.NUMBER:
                    NumberExpression number = new NumberExpression(Integer.parseInt(terminalText));
                    activeExpression.setNumberExpression(number);
                    break;

                case MicroCLexer.IDENTIFIER:
                    IdentifierExpression id = new IdentifierExpression(ScopeController.getVariable(terminalText));
                    activeExpression.setIdentifierExpression(id);
                    break;

                case MicroCLexer.CHARVAL:
                    char c = terminalText.charAt(1);
                    CharacterExpression character = new CharacterExpression(c);
                    activeExpression.setCharacterExpression(character);
                    break;

                case MicroCLexer.BOOLVAL:
                    boolean b = terminalText.equals("true");
                    BooleanExpression bool = new BooleanExpression(b);
                    activeExpression.setBooleanExpression(bool);
                    break;
            }
        }

        // Capture PrefixOP / Create variable to be captured by PostfixOP
        if (terminalType == MicroCLexer.IDENTIFIER) {
            Variable v = ScopeController.getVariable(terminalText);
            if (activePrefix != null){
                activePrefix.variable = v;
            } else {
                activeVariable = v;
            }
        }

        // Capture assignment variable
        if (activeAssignment != null && activeAssignment.variable == null){
            if (terminalType == MicroCLexer.IDENTIFIER) {
                activeAssignment.variable = ScopeController.getVariable(terminalText);
                // System.out.println("assigning to " + terminalText);
            }
        }
    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {
    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {
    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {
    }

}
