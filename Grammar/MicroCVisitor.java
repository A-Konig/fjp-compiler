// Generated from E:/moje/school/04_ctvrtak/fjp/SecondDraft/src/Grammar\MicroC.g4 by ANTLR 4.8
package Grammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MicroCParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MicroCVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MicroCParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#globalVariables}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGlobalVariables(MicroCParser.GlobalVariablesContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(MicroCParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcHeader}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncHeader(MicroCParser.FuncHeaderContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncName(MicroCParser.FuncNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcParamDeclr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#oneParamD}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneParamD(MicroCParser.OneParamDContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncCall(MicroCParser.FuncCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcParamCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncParamCall(MicroCParser.FuncParamCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#oneParamC}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOneParamC(MicroCParser.OneParamCContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#code}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCode(MicroCParser.CodeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#line}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine(MicroCParser.LineContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(MicroCParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(MicroCParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(MicroCParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(MicroCParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#aExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAExpression(MicroCParser.AExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#tExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTExpression(MicroCParser.TExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#kExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKExpression(MicroCParser.KExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#fExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFExpression(MicroCParser.FExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#gExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGExpression(MicroCParser.GExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#arrayExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayExpression(MicroCParser.ArrayExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#arrayPos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayPos(MicroCParser.ArrayPosContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(MicroCParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#ifBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfBlock(MicroCParser.IfBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#forBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForBlock(MicroCParser.ForBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#forDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForDeclaration(MicroCParser.ForDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#forVariableAssign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForVariableAssign(MicroCParser.ForVariableAssignContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#forVariableDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForVariableDecl(MicroCParser.ForVariableDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#whileBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileBlock(MicroCParser.WhileBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#doWhileBlock}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoWhileBlock(MicroCParser.DoWhileBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#conditionLine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionLine(MicroCParser.ConditionLineContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#simpleCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleCondition(MicroCParser.SimpleConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#assignmentOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#arithmOpL1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmOpL1(MicroCParser.ArithmOpL1Context ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#arithmOpL2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArithmOpL2(MicroCParser.ArithmOpL2Context ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#postfixOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfixOperator(MicroCParser.PostfixOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#prefixOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefixOperator(MicroCParser.PrefixOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#comparator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparator(MicroCParser.ComparatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(MicroCParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncType(MicroCParser.FuncTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MicroCParser#funcAccess}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncAccess(MicroCParser.FuncAccessContext ctx);
}