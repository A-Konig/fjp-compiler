package CodeGenerating.Controllers;

public class BrainfuckController {

    public static final StringBuilder PROGRAM = new StringBuilder();
    public static StringBuilder codeTarget = PROGRAM;

    public static int registerStart = 20;

    public static int stackBumper = 50;
    public static int stackTop = 100;

    public static final String slideRight = "+[->+]-";
    public static final String slideLeft = "+[-<+]-";

    public static void pop(int target) {
        int dist = stackBumper - target;

        append(">", stackBumper);

        codeTarget.append(">");
        codeTarget.append(slideRight);
        codeTarget.append(">");

        codeTarget.append("[-");

        codeTarget.append("<<");
        codeTarget.append(slideLeft);
        append((dist < 0) ? ">" : "<", Math.abs(dist));
        codeTarget.append("+");
        append((dist < 0) ? "<" : ">", Math.abs(dist));
        codeTarget.append(">");
        codeTarget.append(slideRight);
        codeTarget.append(">");

        codeTarget.append("]");


        codeTarget.append("<+");
        codeTarget.append(slideLeft);
        append("<", stackBumper);
    }

    public static void push(int source) {
        int dist = stackBumper - source;

        append(">", source);
        codeTarget.append("[-");

        append((dist < 0) ? "<" : ">", Math.abs(dist));
        codeTarget.append(">");
        codeTarget.append(slideRight);
        codeTarget.append("<");
        codeTarget.append("+");
        codeTarget.append("<"); // !!!! ADDED, might not be correct
        codeTarget.append(slideLeft);
        append((dist < 0) ? ">" : "<", Math.abs(dist));

        codeTarget.append("]");

        // Add bumper
        append((dist < 0) ? "<" : ">", Math.abs(dist));
        codeTarget.append(">");
        codeTarget.append(slideRight);
        codeTarget.append("<<-<");
        codeTarget.append(slideLeft);

        append("<", stackBumper);
    }

    public static void append(String symbol, int amount) {
        for (int i = 0; i < amount; i++) {
            codeTarget.append(symbol);
        }
    }

    public static void copy(int source, int target, int target2) {
        // assumed to be staring at 0

        int dist = target - source;
        int dist2 = target2 - target;
        int dist3 = target2 - source;

        // copy from source to both targets
        append( ">", source);

        codeTarget.append("[-");
        append((dist < 0) ? "<" : ">", Math.abs(dist));
        codeTarget.append("+");
        append((dist2 < 0) ? "<" : ">", Math.abs(dist2));
        codeTarget.append("+");
        append((dist3 < 0) ? ">" : "<", Math.abs(dist3));
        codeTarget.append("]");


        // go from source back to target2
        // copy from target2 to source
        append((dist3 < 0) ? "<" : ">", Math.abs(dist3));

        codeTarget.append("[-");
        append((dist3 < 0) ? ">" : "<", Math.abs(dist3));
        codeTarget.append("+");
        append((dist3 < 0) ? "<" : ">", Math.abs(dist3));
        codeTarget.append("]");


        // back to 0
        append("<", Math.abs(target2));
    }

    public static void move(int source, int target) {
        // assumed to be staring at 0

        int dist = target - source;

        // copy from source to target
        append( ">", source);

        codeTarget.append("[-");
        append((dist < 0) ? "<" : ">", Math.abs(dist));
        codeTarget.append("+");
        append((dist < 0) ? ">" : "<", Math.abs(dist));
        codeTarget.append("]");

        append( "<", source);
    }

    public static void initStack() {
        append(">", stackBumper);
        append("-", 1);
        append(">", stackTop - stackBumper);
        append("-", 1);
        append("<", stackTop);
    }

    // MATH OPS

    /**
     * Adds two numbers in reg:0 and reg:1 in Brainfuck, push to stack
     */
    public static void add(){
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("[->+<]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart + 1);
    }

    /**
     * Subtracts two numbers in reg:0 and reg:1 in Brainfuck, push to stack
     */
    public static void sub(){
        BrainfuckController.append(">", BrainfuckController.registerStart + 1);
        BrainfuckController.append("[-<->]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart + 1);
        BrainfuckController.push(BrainfuckController.registerStart);
    }

    /**
     * Multiplies numbers in reg:0 and reg:1 in Brainfuck
     */
    public static void mul(){
        BrainfuckController.append(">", BrainfuckController.registerStart);

        // Reg:0 = X, Reg:1 = Y
        // To multiply X by Y means to add up Y X-times
        // X is the control variable
        // each time copy reg:1 to reg:2 and reg:3
        // reg:2 <- reg:1
        // reg:3 will accumulate the solution
        // clear all after to be sure
        BrainfuckController.append("[->[->+>+<<]>[-<+>]<<][-]>[-]>[-]<<", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart + 3);
    }

    /**
     * Divides numbers in reg:0 and reg:1 in Brainfuck
     */
    public static void div(){
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append(">>+<<", 1);
        BrainfuckController.append("[->-[>+>>]>[[-<+>]+>+>>]<<<<<]", 1);
        BrainfuckController.append(">[-]>[-]<<", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart + 3);
    }

    /**
     * Executes modulo for two numbers in reg:0 and reg:1 in Brainfuck
     */
    public static void mod(){
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append(">>+<<", 1);
        BrainfuckController.append("[->-[>+>>]>[[-<+>]+>+>>]<<<<<]", 1);
        BrainfuckController.append(">[-]>->[-]<<<", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart + 2);
    }

    // Deprecated
    /*

    public static int declareVariable(int[] value, int size){
        int location = assignedMemory;

        // start at 0
        append(">", assignedMemory);

        for (int i = 0; i < size; i++) {
            append("+", value[i]);
            append(">", 1);
        }

        append("<", assignedMemory + size);

        // Arrays require 4 extra spaces at start to index with variables at runtime
        // Other data types are of size 1
        assignedMemory += (size > 1) ? size + 4 : 1;

        return location;
    }


    public static void print(int source) {
        append(">", source);
        codeTarget.append(".");
        append("<", source);
    }

    public static void put(int target, int value) {
        // assume start on 0
        // assume target is 0 or we are adding
        append(">", target);
        append("+", value);
        append("<", target);
    }

    public static void clearAndPut(int target, int value) {
        // assume start on 0
        append(">", target);
        append("[-]", 1);
        append("+", value);
        append("<", target);
    }



    */
}
