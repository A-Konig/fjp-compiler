package SemanticAnalysis;

import Main.Function;
import Main.Variable;
import Main.VariableType;

import java.io.Console;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Class used for semantic analysis - declaring variables, functions, using variables...
 */
public class Analysator {

    /** Symbol table */
    private Hashtable<String, Variable> symbolTable;
    /** Function table */
    public Hashtable<String, Function> funcTable;
    /** List of errors encountered during semantic analysis */
    public ArrayList<String> errors;
    /** Was an error found in last processed line */
    boolean foundErrorInLine;

    /** Keys to temporary block variables */
    private ArrayList<ArrayList<String>> blockVariables;
    /** Path to current block */
    private String blockPath;
    /** Depth of block recursion */
    public int blockLayer;

    /**
     * Constructor
     */
    Analysator() {
        funcTable = new Hashtable<>();
        symbolTable = new Hashtable<>();
        errors = new ArrayList<>();

        blockVariables = new ArrayList<>();
        blockPath = "";
        blockLayer = 0;

        // add predefined functions
        Function print = new Function();
        print.name = "printf";
        Variable x = new Variable();
        x.name = "x";
        x.functionName = "printf";
        print.parameters.add(x);

        Function read = new Function();
        read.name = "getchar";
        Variable y = new Variable();
        y.name = "y";
        y.functionName = "getchar";
        read.parameters.add(y);

        funcTable.put("printf", print);
        funcTable.put("getchar", read);
    }

    /**
     * Adding new error to error list
     * @param error error text
     */
    void addError(String error) {
        errors.add(error);
        foundErrorInLine = true;
    }

    /**
     * Adding new error to error list
     * @param error error text
     */
    void addPostError(String error) {
        errors.add(error);
        foundErrorInLine = false;
    }

    /**
     * Function called when starting a new block of code
     */
    void StartBlock() {
        blockVariables.add(new ArrayList<>());
        blockLayer++;
        blockPath += ".block";
    }


    /**
     * Adding a new variable key to a list containing block variables
     * These variables will be deleted after the block ends
     * @param path      path to variable
     * @param variable  variable name
     */
    void AddVariableToBlock(String variable, String path) {

        if (blockVariables.size() > 0) {
            ArrayList<String> vars = blockVariables.get(blockVariables.size() - 1);
            vars.add(path + blockPath + "." +  variable);
        }
    }

    /**
     * Deleting block variables from symbol table
     */
    void RollbackAfterBlock() {
        if (blockVariables.size() > 0) {
            ArrayList<String> vars = blockVariables.get(blockVariables.size() - 1);

            for (int i = 0; i < vars.size(); i++) {
                symbolTable.remove(vars.get(i));
            }
            blockPath = blockPath.substring(0, blockPath.length() - 6);

            blockVariables.remove(blockVariables.size() - 1);
        }
        blockLayer--;
    }

    /**
     * Declare new function
     * @param name  name of function
     */
    void DeclareFunc(String name) {
        if (name.equals("global"))
            addError("Function called 'global' cannot be created, it is a reserved word.");

        Function f = new Function();
        f.name = name;
        if (funcTable.containsKey(f.name))
            addError(f.name + " already declared");
        else
            funcTable.put(name, f);

    }

    /**
     * Declare new variable
     * @param name      name of variable
     * @param function  function that the variable is declared in
     * @param type      type of variable
     */
    boolean DeclareVar(String name, String function, VariableType type, boolean isArray) {
        if (function.length() == 0)
            function = "global";

        if (!symbolTable.containsKey(function + blockPath + "." + name)) {
            Variable v = new Variable();
            v.type = type;
            v.name = name;
            v.array = isArray;
            v.functionName = function;

            symbolTable.put(function + blockPath + "." + name, v);

            return true;
        }
        else {
            addError(name + " already declared!");
            return false;
        }
    }

    /**
     * Checks if variable exists or not
     * @param name          name of variable
     * @param function      current function
     * @param canBeArray    can variable be an array
     * @return              found Variable
     */
    Variable CheckExistenceVariable(String name, String function, boolean canBeArray) {


        Variable v = GetVariable(name, function);
        if (v == null)
            return null;

        if (canBeArray != v.array) {
            addError("Invalid operation with variable " + name);
            return null;
        }

        return v;
    }

    /**
     * Is it possible to assign value to variable
     * @param name      name of variable
     * @param function  current function
     * @return          true if assigned correctly
     */
    boolean AssigningVariable(String name, String function, boolean isArray) {
        // exists in current function or in global
        Variable v = GetVariable(name, function);

        if (v == null) {
            addError(name + " not declared!");
            return false;
        }

        if (isArray && !v.array) {
            addError("Incorrect usage of [] in variable " + name);
            return false;
        }

        return true;
    }

    /**
     * Assign value to a position in an array. Not exceeding lenght of an array is not checked.
     * @param arrName   name of array
     * @param function  current function
     * @param posName   variable representing the position
     */
    void SetArrayPos(String arrName, String function, String posName) {
        Variable v = GetVariable(arrName, function);
        if (v == null)
            return;

        if (!v.array)
            errors.add("Incorrect usage of []");
    }

    /**
     * Assign value to a position in an array. Not exceeding length of an array is not checked.
     * @param arrName   name of array
     * @param function  current function
     * @param pos       position in array
     */
    void SetArrayPos(String arrName, String function, int pos) {
        Variable v = GetVariable(arrName, function);
        if (v == null)
            return;

        if (v.array) {
            if (pos < 0)
                errors.add("Trying to assign to an invalid array position");
        } else
            errors.add("Incorrect usage of []");
    }

    /**
     * Add parameter to function
     * @param funcName      name of function
     * @param paramName     name of parameter
     * @param currType      type of parameter
     */
    void AddParameter(String funcName, String paramName, VariableType currType, boolean isArray) {
        Function f = funcTable.get(funcName);
        Variable v = new Variable();
        v.type = currType;
        v.name = paramName;
        v.array = isArray;
        f.parameters.add(v);

        symbolTable.put(funcName + "." + paramName, v);
    }

    /**
     * Get variable from symbol table. First looks in current block, then in current function, then into global variables.
     * @param name      name of variable
     * @param currFunc  current function
     * @return          returns found variable or null
     */
    Variable GetVariable(String name, String currFunc) {
        //check all superior blocks
        String currBlockPath = "";
        for (int i = 0; i < blockLayer; i++) {
            currBlockPath += ".block";
        }

        for (int i = 0; i < blockLayer; i++) {
            if (symbolTable.containsKey(currFunc + currBlockPath + "." + name))
                return symbolTable.get(currFunc + currBlockPath + "." + name);
            currBlockPath = currBlockPath.substring(0, currBlockPath.length()-6);
        }

        // check function and global variables
        if (symbolTable.containsKey(currFunc + "." + name))
            return symbolTable.get(currFunc + "." + name);
        else if (symbolTable.containsKey("global." + name)) {
            Variable v = symbolTable.get("global." + name);
            return v;
        }

        addError("Variable " + name + " does not exist.");
        return null;
    }

    /**
     * Check if function was called correctly
     * @param function      name of function
     * @param callParams    parameters with which it was called
     */
    void CheckFunctionCall(String function, String currFunc, ArrayList<Variable> callParams) {
        // does function exist
        if (funcTable.containsKey(function)) {
            if (function.equals(currFunc)) {
                addError("Recursion is prohibited.");
                return;
            }

            Function f = funcTable.get(function);
            ArrayList<Variable> params = f.parameters;

            // go through and find out if they match
            if (params.size() != callParams.size()) {
                addError("Incorrect number of parameters when calling function " + function + " " + params.size() + " required " + " " + callParams.size() +" given");
            } else {
                for (int i = 0; i < params.size(); i++) {
                    Variable v1 = params.get(i);
                    Variable v2 = callParams.get(i);

                    if (v1.array != v2.array)
                        addError("Calling with incorrect variable type, trying to pass non-array as array");
                }
            }

        } else {
            addError("Calling function that does not exist " + function);
        }
    }

}