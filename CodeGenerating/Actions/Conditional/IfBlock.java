package CodeGenerating.Actions.Conditional;

import CodeGenerating.Controllers.BrainfuckController;

/**
 * Describes and evaluates if blocks
 *
 * @author kacerekz
 */
public class IfBlock extends ConditionalBlock {

    /**
     * Generate IfBlock code
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Process condition and append generated code
        condition.executeAction(null);
        BrainfuckController.append(condition.getActionCode(), 1);

        // Condition result is on the stack
        // Pop result to reg0
        // Move to reg0
        BrainfuckController.pop(BrainfuckController.registerStart);
        BrainfuckController.append(">", BrainfuckController.registerStart);

        // Execute conditionally (pointer on register 0)
        // - memory either 0 or 1
        // - if 0 : register clear, BUT we have to always end on memory 0 so we always need to return,
        //   therefore after conditional code we must go back to register 0 so we can return unconditionally
        // - if 1 : clear register, return to memory 0, execute conditional code, return to register 0
        //   return to memory 0 unconditionally

        BrainfuckController.append("[", 1);
        BrainfuckController.append("[-]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);

        // Append conditional code
        BrainfuckController.append(nestedCode, 1);

        // There will be a zero in reg0: end loop with it
        // MUST move to reg0 here
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("]", 1);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        // End at memory 0
    }

}
