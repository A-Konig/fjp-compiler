package Main;

/** Variable types */
public enum VariableType {
    INTEGER,
    BOOL,
    CHARACTER;
}
