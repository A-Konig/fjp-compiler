package Main;

import java.util.ArrayList;

/**
 * Class used for the storage of functions during analysis of code
 */
public class Function {
    /** Name of function */
    public String name;
    /** function parameters */
    public ArrayList<Variable> parameters;
    /** function variables */
    public ArrayList<Variable> allVariables;

    /** Constructor */
    public Function() {
        parameters = new ArrayList<Variable>();
        allVariables = new ArrayList<Variable>();
    }
}
