package CodeGenerating.Controllers;

import CodeGenerating.Actions.Action;

import java.util.Stack;

/**
 * Provides functions to manage code generation
 * Code is generated within the context of its block or action
 * These actions live on a stack, which makes assigning code to their parent blocks easier
 *
 * @author kacerekz
 */
public class FlowController {

    /** Action stack */
    private static Stack<Action> actionStack;

    /** Static init */
    static {
        actionStack = new Stack<>();
    }

    /**
     * Push an action to the action stack and generate new code to its context
     * @param a Action
     */
    public static void push(Action a){
        actionStack.push(a);
        BrainfuckController.codeTarget = a.codeTarget;
    }

    /**
     * Pop action and return context to its parent
     */
    public static void pop(){
        actionStack.pop();
        BrainfuckController.codeTarget = (actionStack.isEmpty())
                ? BrainfuckController.PROGRAM
                : actionStack.peek().codeTarget;
    }
}
