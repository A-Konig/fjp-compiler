package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Expressions.Operations.ArithmeticOP1;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.OPTypeArithmetic1;

/**
 * Type A Expression
 *
 * @author kacerekz
 */
public class ExpressionA extends Expression {

    /** Left side expression */
    public ExpressionA left;

    /** Right side expression */
    public ExpressionT right;

    /** Level 1 arithmetic operation */
    public ArithmeticOP1 op;

    /**
     * Generates code to calculate this expression
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        if (op == null) {
            right.executeAction(null);
            BrainfuckController.append(right.getActionCode(), 1);

        } else {
            // Evaluate left and right branch
            left.executeAction(null);
            right.executeAction(null);
            BrainfuckController.append(left.getActionCode(), 1);
            BrainfuckController.append(right.getActionCode(), 1);

            // Execute operation
            op.executeAction(null);
            BrainfuckController.append(op.getActionCode(), 1);
        }
    }

    /**
     * Set the left side
     * @param e Type A Expression
     */
    @Override
    public void setExpressionA(ExpressionA e) {
        left = e;
    }

    /**
     * Set the right side
     * @param e Type T Expression
     */
    @Override
    public void setExpressionT(ExpressionT e) {
        right = e;
    }

    /**
     * Set the operation
     * @param op Level 1 Arithmetic operation
     */
    @Override
    public void setArithmeticOP1(ArithmeticOP1 op) {
        this.op = op;
    }
}
