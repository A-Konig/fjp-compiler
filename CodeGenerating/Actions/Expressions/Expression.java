package CodeGenerating.Actions.Expressions;

import CodeGenerating.Actions.Action;
import CodeGenerating.Actions.Expressions.Operations.*;
import CodeGenerating.Actions.Expressions.Values.*;

/**
 * Expression base class
 *
 * @author kacerekz
 */
public abstract class Expression extends Action {
    /** Parent expression in the expression tree */
    public Expression parent;

    /**
     * Set an Expression - Override in classes that accept this type
     * @param e Type A Expression
     */
    public void setExpressionA(ExpressionA e){}

    /**
     * Set an Expression - Override in classes that accept this type
     * @param e Type F Expression
     */
    public void setExpressionF(ExpressionF e){}

    /**
     * Set an Expression - Override in classes that accept this type
     * @param e Type G Expression
     */
    public void setExpressionG(ExpressionG e){}

    /**
     * Set an Expression - Override in classes that accept this type
     * @param e Type K Expression
     */
    public void setExpressionK(ExpressionK e){}

    /**
     * Set an Expression - Override in classes that accept this type
     * @param e Type T Expression
     */
    public void setExpressionT(ExpressionT e){}


    /**
     * Set an Operation - Override in classes that accept this type
     * @param op Level 1 Arithmetic operation
     */
    public void setArithmeticOP1(ArithmeticOP1 op){}

    /**
     * Set an Operation - Override in classes that accept this type
     * @param op Level 2 Arithmetic operation
     */
    public void setArithmeticOP2(ArithmeticOP2 op){}

    /**
     * Set an Operation - Override in classes that accept this type
     * @param op A Prefix operation
     */
    public void setPrefixOP(PrefixOP op){}

    /**
     * Set an Operation - Override in classes that accept this type
     * @param op A Postfix operation
     */
    public void setPostfixOP(PostfixOP op){}


    /**
     * Set a value - Override in classes that accept this type
     * @param e An Array value
     */
    public void setArrayExpression(ArrayExpression e){}

    /**
     * Set a value - Override in classes that accept this type
     * @param e A boolean value
     */
    public void setBooleanExpression(BooleanExpression e){}

    /**
     * Set a value - Override in classes that accept this type
     * @param e A character value
     */
    public void setCharacterExpression(CharacterExpression e){}

    /**
     * Set a value - Override in classes that accept this type
     * @param e An identifier
     */
    public void setIdentifierExpression(IdentifierExpression e){}

    /**
     * Set a value - Override in classes that accept this type
     * @param e A number value
     */
    public void setNumberExpression(NumberExpression e){}

}
