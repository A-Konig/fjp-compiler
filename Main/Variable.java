package Main;

/**
 * Class used for the storage of a variable during analysis of code
 */public class Variable {

    /** In which function is given variable defined */
    public String functionName;

    /** Name of variable */
    public String name;
    /** Type of variable */
    public VariableType type;
    /** Location where it is stored */
    public int location;

    /** Is variable an array */
    public boolean array;
    /** Length of array (if variable not an array, it is of length 1) */
    public int arrayLength ;
    /** Value of variable */
    public int value;

    /** Constructor */
    public Variable() {
        arrayLength = 1;
        array = false;
    }
}
