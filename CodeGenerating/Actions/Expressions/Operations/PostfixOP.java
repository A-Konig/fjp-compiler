package CodeGenerating.Actions.Expressions.Operations;

import CodeGenerating.Actions.Action;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.OPTypePostfix;
import CodeGenerating.Types.OPTypePrefix;
import Main.Variable;

/**
 * Represents a postfix operation
 */
public class PostfixOP extends Action {

    /** Operation type */
    public OPTypePostfix type;

    /** Variable */
    public Variable variable;

    /**
     * Create a postfix operation
     * @param type Operation type
     */
    public PostfixOP(OPTypePostfix type){
        this.type = type;
    }

    /**
     * Execute a postfix operation
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // First push the variable
        BrainfuckController.copy(variable.location, BrainfuckController.registerStart, BrainfuckController.registerStart + 1);
        BrainfuckController.push(BrainfuckController.registerStart);

        // Then go to the variable and change it
        BrainfuckController.append(">", variable.location);
        BrainfuckController.append((type == OPTypePostfix.OP_ADD) ? "+" : "-", 1);
        BrainfuckController.append("<", variable.location);
    }

    /**
     * Set variable
     * @param variable Variable
     */
    public void setVariable(Variable variable){
        this.variable = variable;
    }
}
