package CodeGenerating.Actions.Expressions.Values;

import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Represents a character expression
 *
 * @author kacerekz
 */
public class CharacterExpression extends Expression {

    /** Character value */
    public char value;

    /**
     * Create Character expression of given value
     * @param value Character value
     */
    public CharacterExpression(char value){
        this.value = value;
    }

    /**
     * Pushes a character value to the Brainfuck stack
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Go to reg:0
        // Put character value
        // Push to stack from reg:0
        // End at mem:0
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("+", value);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart);
    }
}
