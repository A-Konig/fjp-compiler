grammar MicroC;

// start
primaryExpression
    :	/* empty */
    | globalVariables
	| primaryExpression function
	;

// global variables
globalVariables
    :
    | declaration globalVariables
    ;

// function declaration / call
function
    : funcHeader '{' code '}'
    ;

funcHeader
    : funcAccess? funcType funcName '(' funcParamDeclr ')'
    ;

funcName
    : IDENTIFIER
    ;

funcParamDeclr
    :
    | oneParamD ','? funcParamDeclr
    ;

oneParamD
    : type IDENTIFIER
    | type arrayExpression
    ;

funcCall
    : IDENTIFIER '(' funcParamCall ')' ';'
    ;

funcParamCall
    :
    | oneParamC
    | oneParamC ',' funcParamCall
    ;

oneParamC
    : IDENTIFIER
    | NUMBER
    | CHARVAL
    | BOOLVAL
    | arrayExpression
    ;

// code
code
    :
    | code line
    | code block
    ;

line
    : declaration
    | assignment
    | funcCall
	;

value
    : NUMBER | IDENTIFIER | CHARVAL
    | arrayExpression
    ;

// declaration
declaration
    : type IDENTIFIER ';'
    | type arrayExpression ';'
    | type IDENTIFIER '=' expression ';'
    ;

// assignment
assignment
    :
    IDENTIFIER assignmentOperator expression ';'
    | arrayExpression assignmentOperator expression ';'
    ;

// arithmetic

expression
    : aExpression
    ;

aExpression
    : tExpression
    | aExpression arithmOpL1 tExpression
    ;

tExpression
    : kExpression
    | tExpression arithmOpL2 kExpression
    ;

kExpression
    : fExpression
    | prefixOperator fExpression
    ;

fExpression
    : gExpression
    | gExpression postfixOperator
    ;

gExpression
    : '(' aExpression ')'
    | NUMBER
    | IDENTIFIER
    | BOOLVAL
    | CHARVAL
    | arrayExpression
    ;

// array

arrayExpression
    : IDENTIFIER arrayPos
    ;

arrayPos
    : '[' value ']'
    ;

// block

block
    : ifBlock
    | forBlock
    | whileBlock
    | doWhileBlock
    ;

ifBlock:
    'if' conditionLine '{' code '}'
    ;

forBlock
    : 'for' '(' forDeclaration ')' '{'  code '}'
    ;

forDeclaration
    : forVariableDecl ';' simpleCondition ';' forVariableAssign
    ;

forVariableAssign
    : IDENTIFIER assignmentOperator expression
    | arrayExpression assignmentOperator expression
    ;

forVariableDecl
    : IDENTIFIER '=' expression
    | arrayExpression '=' expression
    ;

whileBlock
    : 'while' '(' conditionLine ')' '{' code '}'
    ;

doWhileBlock
    : 'do' '{' code '}' 'while' '(' conditionLine ')' ';'
    ;

// condition
conditionLine
    : simpleCondition
    | '(' simpleCondition ')'
    ;

simpleCondition
    : expression comparator expression
    | expression
    ;

// assignment
assignmentOperator
    : '=' | '*=' | '/=' | '%=' | '+=' | '-='
    ;

arithmOpL1
    : '+' | '-'
    ;

arithmOpL2
    : '*' | '/' | '%'
    ;

postfixOperator
    : '++' | '--'
    ;

prefixOperator
    : '++' | '--'
    ;

comparator
    : '<' | '>' | '>=' | '<=' | '!=' | '=='
    ;

// reserved words

type
    : INT | BOOL | CHAR;

funcType
    : VOID;

funcAccess
    : PUBLIC | PRIVATE;

// TYPES

INT: 'int' | 'integer';
BOOL: 'bool' | '_Bool';
CHAR: 'char';

VOID: 'void';
PUBLIC: 'public';
PRIVATE: 'private';
BOOLVAL: 'true' | 'false';

// NAMES
NUMBER: Digit+;
IDENTIFIER: Letter+;
CHARVAL: '\'' [ -~] '\'';

// USING
Digit: [0-9];
Letter: [a-zA-Z0-9_];

// SKIPS
Whitespace: [ \t\r]+
        -> skip
    ;

Newline: ( '\r'+ '\n'? | '\n'+ )
        -> skip
    ;

BlockComment: '/*' .*? '*/'
        -> skip
    ;

LineComment: '//' ~[\r\n]*
        -> skip
    ;