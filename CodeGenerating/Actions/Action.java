package CodeGenerating.Actions;

import CodeGenerating.Controllers.FlowController;

/**
 * Represents an action that can be converted to Brainfuck code
 *
 * @author kacerekz
 */
public abstract class Action {

    /** An action must have a target to which its code is written */
    public StringBuilder codeTarget = new StringBuilder();

    /**
     * Flow compliant action execution
     * @param nestedCode Code to nest
     */
    public void executeAction(String nestedCode){
        FlowController.push(this);
        executeDirectAction(nestedCode);
        FlowController.pop();
    }

    /**
     * An action must be executable
     * @param nestedCode Code to be nested within the action
     */
    public abstract void executeDirectAction(String nestedCode);

    /**
     * Returns the code target of this action
     * @return Target of this action's code
     */
    public String getActionCode() {
        return (codeTarget == null) ? "" : codeTarget.toString();
    }
}
