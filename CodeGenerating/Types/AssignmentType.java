package CodeGenerating.Types;

/**
 * Enum of assignment operators
 *
 * @author kacerekz
 */
public enum AssignmentType {
    // '=' | '*=' | '/=' | '%=' | '+=' | '-='
    EQUALS,
    TIMES_EQUALS,
    DIV_EQUALS,
    MOD_EQUALS,
    PLUS_EQUALS,
    MINUS_EQUALS
}
