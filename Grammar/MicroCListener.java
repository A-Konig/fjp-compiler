// Generated from E:/moje/school/04_ctvrtak/fjp/SecondDraft/src/Grammar\MicroC.g4 by ANTLR 4.8
package Grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MicroCParser}.
 */
public interface MicroCListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MicroCParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpression(MicroCParser.PrimaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#globalVariables}.
	 * @param ctx the parse tree
	 */
	void enterGlobalVariables(MicroCParser.GlobalVariablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#globalVariables}.
	 * @param ctx the parse tree
	 */
	void exitGlobalVariables(MicroCParser.GlobalVariablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(MicroCParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(MicroCParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcHeader}.
	 * @param ctx the parse tree
	 */
	void enterFuncHeader(MicroCParser.FuncHeaderContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcHeader}.
	 * @param ctx the parse tree
	 */
	void exitFuncHeader(MicroCParser.FuncHeaderContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcName}.
	 * @param ctx the parse tree
	 */
	void enterFuncName(MicroCParser.FuncNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcName}.
	 * @param ctx the parse tree
	 */
	void exitFuncName(MicroCParser.FuncNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcParamDeclr}.
	 * @param ctx the parse tree
	 */
	void enterFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcParamDeclr}.
	 * @param ctx the parse tree
	 */
	void exitFuncParamDeclr(MicroCParser.FuncParamDeclrContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#oneParamD}.
	 * @param ctx the parse tree
	 */
	void enterOneParamD(MicroCParser.OneParamDContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#oneParamD}.
	 * @param ctx the parse tree
	 */
	void exitOneParamD(MicroCParser.OneParamDContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void enterFuncCall(MicroCParser.FuncCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void exitFuncCall(MicroCParser.FuncCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcParamCall}.
	 * @param ctx the parse tree
	 */
	void enterFuncParamCall(MicroCParser.FuncParamCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcParamCall}.
	 * @param ctx the parse tree
	 */
	void exitFuncParamCall(MicroCParser.FuncParamCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#oneParamC}.
	 * @param ctx the parse tree
	 */
	void enterOneParamC(MicroCParser.OneParamCContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#oneParamC}.
	 * @param ctx the parse tree
	 */
	void exitOneParamC(MicroCParser.OneParamCContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#code}.
	 * @param ctx the parse tree
	 */
	void enterCode(MicroCParser.CodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#code}.
	 * @param ctx the parse tree
	 */
	void exitCode(MicroCParser.CodeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#line}.
	 * @param ctx the parse tree
	 */
	void enterLine(MicroCParser.LineContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#line}.
	 * @param ctx the parse tree
	 */
	void exitLine(MicroCParser.LineContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(MicroCParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(MicroCParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(MicroCParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(MicroCParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(MicroCParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(MicroCParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(MicroCParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(MicroCParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#aExpression}.
	 * @param ctx the parse tree
	 */
	void enterAExpression(MicroCParser.AExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#aExpression}.
	 * @param ctx the parse tree
	 */
	void exitAExpression(MicroCParser.AExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#tExpression}.
	 * @param ctx the parse tree
	 */
	void enterTExpression(MicroCParser.TExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#tExpression}.
	 * @param ctx the parse tree
	 */
	void exitTExpression(MicroCParser.TExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#kExpression}.
	 * @param ctx the parse tree
	 */
	void enterKExpression(MicroCParser.KExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#kExpression}.
	 * @param ctx the parse tree
	 */
	void exitKExpression(MicroCParser.KExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#fExpression}.
	 * @param ctx the parse tree
	 */
	void enterFExpression(MicroCParser.FExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#fExpression}.
	 * @param ctx the parse tree
	 */
	void exitFExpression(MicroCParser.FExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#gExpression}.
	 * @param ctx the parse tree
	 */
	void enterGExpression(MicroCParser.GExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#gExpression}.
	 * @param ctx the parse tree
	 */
	void exitGExpression(MicroCParser.GExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#arrayExpression}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpression(MicroCParser.ArrayExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#arrayExpression}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpression(MicroCParser.ArrayExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#arrayPos}.
	 * @param ctx the parse tree
	 */
	void enterArrayPos(MicroCParser.ArrayPosContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#arrayPos}.
	 * @param ctx the parse tree
	 */
	void exitArrayPos(MicroCParser.ArrayPosContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(MicroCParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(MicroCParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void enterIfBlock(MicroCParser.IfBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void exitIfBlock(MicroCParser.IfBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#forBlock}.
	 * @param ctx the parse tree
	 */
	void enterForBlock(MicroCParser.ForBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#forBlock}.
	 * @param ctx the parse tree
	 */
	void exitForBlock(MicroCParser.ForBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#forDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterForDeclaration(MicroCParser.ForDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#forDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitForDeclaration(MicroCParser.ForDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#forVariableAssign}.
	 * @param ctx the parse tree
	 */
	void enterForVariableAssign(MicroCParser.ForVariableAssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#forVariableAssign}.
	 * @param ctx the parse tree
	 */
	void exitForVariableAssign(MicroCParser.ForVariableAssignContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#forVariableDecl}.
	 * @param ctx the parse tree
	 */
	void enterForVariableDecl(MicroCParser.ForVariableDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#forVariableDecl}.
	 * @param ctx the parse tree
	 */
	void exitForVariableDecl(MicroCParser.ForVariableDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#whileBlock}.
	 * @param ctx the parse tree
	 */
	void enterWhileBlock(MicroCParser.WhileBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#whileBlock}.
	 * @param ctx the parse tree
	 */
	void exitWhileBlock(MicroCParser.WhileBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#doWhileBlock}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileBlock(MicroCParser.DoWhileBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#doWhileBlock}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileBlock(MicroCParser.DoWhileBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#conditionLine}.
	 * @param ctx the parse tree
	 */
	void enterConditionLine(MicroCParser.ConditionLineContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#conditionLine}.
	 * @param ctx the parse tree
	 */
	void exitConditionLine(MicroCParser.ConditionLineContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#simpleCondition}.
	 * @param ctx the parse tree
	 */
	void enterSimpleCondition(MicroCParser.SimpleConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#simpleCondition}.
	 * @param ctx the parse tree
	 */
	void exitSimpleCondition(MicroCParser.SimpleConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(MicroCParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#arithmOpL1}.
	 * @param ctx the parse tree
	 */
	void enterArithmOpL1(MicroCParser.ArithmOpL1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#arithmOpL1}.
	 * @param ctx the parse tree
	 */
	void exitArithmOpL1(MicroCParser.ArithmOpL1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#arithmOpL2}.
	 * @param ctx the parse tree
	 */
	void enterArithmOpL2(MicroCParser.ArithmOpL2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#arithmOpL2}.
	 * @param ctx the parse tree
	 */
	void exitArithmOpL2(MicroCParser.ArithmOpL2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#postfixOperator}.
	 * @param ctx the parse tree
	 */
	void enterPostfixOperator(MicroCParser.PostfixOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#postfixOperator}.
	 * @param ctx the parse tree
	 */
	void exitPostfixOperator(MicroCParser.PostfixOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#prefixOperator}.
	 * @param ctx the parse tree
	 */
	void enterPrefixOperator(MicroCParser.PrefixOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#prefixOperator}.
	 * @param ctx the parse tree
	 */
	void exitPrefixOperator(MicroCParser.PrefixOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#comparator}.
	 * @param ctx the parse tree
	 */
	void enterComparator(MicroCParser.ComparatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#comparator}.
	 * @param ctx the parse tree
	 */
	void exitComparator(MicroCParser.ComparatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MicroCParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MicroCParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcType}.
	 * @param ctx the parse tree
	 */
	void enterFuncType(MicroCParser.FuncTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcType}.
	 * @param ctx the parse tree
	 */
	void exitFuncType(MicroCParser.FuncTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MicroCParser#funcAccess}.
	 * @param ctx the parse tree
	 */
	void enterFuncAccess(MicroCParser.FuncAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link MicroCParser#funcAccess}.
	 * @param ctx the parse tree
	 */
	void exitFuncAccess(MicroCParser.FuncAccessContext ctx);
}