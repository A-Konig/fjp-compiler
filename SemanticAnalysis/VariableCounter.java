package SemanticAnalysis;

import Main.Function;
import Main.Variable;
import Main.VariableType;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Class that creates a hashtable containing all function in given block of code.
 * Every function contains a table with all declared variables and their array length in its code.
 * Variable that is not an array has array length equal to 1.
 *
 */
public class VariableCounter {
    /** Function table */
    public Hashtable<String, Function> funcTable;

    /** Currently in a for/while/if block */
    private boolean inBlock = false;
    /** Found number of blocks in text */
    private int blockCount = 0;
    /** Currently in block number */
    private int currentBlock = 0;
    /** Current nesting of blocks */
    private ArrayList<Integer> blockHistory = new ArrayList<>();

    /**
     * Constructor
     */
    public VariableCounter() {
        funcTable = new Hashtable<>();

        Function f = new Function();
        f.name = "global";
        funcTable.put("global", f);

        // add predefined functions
        Function print = new Function();
        print.name = "printf";
        Variable x = new Variable();
        x.name = "x";
        x.functionName = "printf";
        print.parameters.add(x);

        Function read = new Function();
        read.name = "getchar";
        Variable y = new Variable();
        y.name = "y";
        y.functionName = "getchar";
        read.parameters.add(y);

        funcTable.put("printf", print);
        funcTable.put("getchar", read);

        blockHistory.add(0);
    }

    /**
     * Entering new block of code
     */
    public void EnterNewBlock() {
        inBlock = true;

        if (currentBlock != 0)
            blockHistory.add(currentBlock);
        blockCount++;
        currentBlock = blockCount;
    }

    /**
     * Exiting current block of code
     */
    public void ExitBlock() {
        currentBlock = blockHistory.get(blockHistory.size() - 1);

        if (currentBlock == 0) {
            inBlock = false;
        } else {
            inBlock = true;
            blockHistory.remove(blockHistory.size() - 1);
        }
    }

    /**
     * Declare new function
     * @param name  name of function
     */
    public void DeclareFunc(String name) {
        Function f = new Function();
        f.name = name;
        funcTable.put(name, f);
    }


    // add variable to function
    /**
     * Declare new variable
     * @param name      name of variable
     * @param function  function that the variable is declared in
     * @param type      type of variable
     */
    public void DeclareVar(String name, String function, VariableType type) {
        Variable v = new Variable();
        v.type = type;
        v.name = name;
        v.functionName = function;

        if (inBlock) {
            v.name = "block" + currentBlock + "." + name;
        } else
            v.name = name;

        // get current function
        Function f = null;
        if (function.length() == 0)
            f = funcTable.get("global");
        else
            f = funcTable.get(function);

        f.allVariables.add(v);
    }

    /**
     * Declare new variable
     * @param name      name of variable
     * @param function  function that the variable is declared in
     * @param type      type of variable
     */
    public void DeclareArrayVar(String name, String function, int varLength, VariableType type) {
        Variable v = new Variable();
        v.type = type;
        v.functionName = function;
        v.array = true;
        v.arrayLength = varLength;

        if (inBlock) {
            v.name = "block" + currentBlock + "." + name;
        } else
            v.name = name;

        // get current function
        Function f = null;
        if (function.length() == 0)
            f = funcTable.get("global");
        else
            f = funcTable.get(function);

        f.allVariables.add(v);
    }

    /**
     * Add parameter to function
     * @param funcName      name of function
     * @param paramName     name of parameter
     * @param currType      type of parameter
     */
    public void AddParameter(String funcName, String paramName, VariableType currType) {
        Function f = funcTable.get(funcName);
        Variable v = new Variable();
        v.type = currType;
        v.name = paramName;
        f.parameters.add(v);
    }

    /**
     * Set last parameter of function as array
     * @param funcName      name of function
     * @param lenght        length of array
     */
    public void SetLastParamAsArray(String funcName, int lenght) {
        Function f = funcTable.get(funcName);
        Variable v = f.parameters.get(f.parameters.size()-1);
        v.array = true;
        v.arrayLength = lenght;
    }

}
