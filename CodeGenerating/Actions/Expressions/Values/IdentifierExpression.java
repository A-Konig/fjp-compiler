package CodeGenerating.Actions.Expressions.Values;

import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.BrainfuckController;
import Main.Variable;

/**
 * Represents a variable expression
 *
 * @author kacerekz
 */
public class IdentifierExpression extends Expression {

    /** A variable */
    public Variable value;

    /**
     * Create Identifier Expression with given value
     * @param value Variable
     */
    public IdentifierExpression(Variable value){
        this.value = value;
    }

    /**
     * Pushes the value of a variable to the Brainfuck stack
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Go to variable location
        // Copy it out to reg:0 and reg:1
        // Copy reg:0 to variable location
        // Push reg:1 to stack

        BrainfuckController.copy(value.location, BrainfuckController.registerStart, BrainfuckController.registerStart + 1);
        BrainfuckController.push(BrainfuckController.registerStart);
    }
}
