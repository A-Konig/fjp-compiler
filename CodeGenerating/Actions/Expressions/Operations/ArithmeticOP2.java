package CodeGenerating.Actions.Expressions.Operations;

import CodeGenerating.Actions.Action;
import CodeGenerating.Controllers.BrainfuckController;
import CodeGenerating.Types.OPTypeArithmetic2;

/**
 * Represents a level 2 arithmetic operation
 *
 * @author kacerekz
 */
public class ArithmeticOP2 extends Action {

    /** Operation type */
    public OPTypeArithmetic2 type;

    /**
     * Create a level 2 arithmetic operation
     * @param type Operation type
     */
    public ArithmeticOP2(OPTypeArithmetic2 type){
        this.type = type;
    }

    /**
     * Generates Brainfuck code for a level 2 operation
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // On stack, right is on top
        BrainfuckController.pop(BrainfuckController.registerStart + 1);
        BrainfuckController.pop(BrainfuckController.registerStart + 0);

        switch (type) {
            case OP_MUL:
                BrainfuckController.mul();
                break;
            case OP_DIV:
                BrainfuckController.div();
                break;
            case OP_MOD:
                BrainfuckController.mod();
                break;
        }
    }
}
