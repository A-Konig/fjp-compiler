package CodeGenerating.Actions;

/**
 * Only used as an alias specifically during for cycle parsing
 *
 * @author kacerekz
 */
public class ForEndAssignment extends Assignment {
    // An Assignment alias
}
