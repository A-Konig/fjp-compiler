package CodeGenerating.Actions.Expressions.Values;

import CodeGenerating.Actions.Expressions.Expression;
import CodeGenerating.Controllers.BrainfuckController;

/**
 * Represents a boolean expression
 *
 * @author kacerekz
 */
public class BooleanExpression extends Expression {

    /** Boolean expression value */
    public boolean value;

    /**
     * Create Boolean Expression of given value
     * @param value Boolean value
     */
    public BooleanExpression(boolean value){
        this.value = value;
    }

    /**
     * Pushes a boolean value to the Brainfuck stack
     * @param nestedCode Code to be nested within the action
     */
    @Override
    public void executeDirectAction(String nestedCode) {
        // Go to reg:0
        // Put 1 if true, 0 in false
        // Push to stack from reg:0
        // End at mem:0
        BrainfuckController.append(">", BrainfuckController.registerStart);
        BrainfuckController.append("+", (value) ? 1 : 0);
        BrainfuckController.append("<", BrainfuckController.registerStart);
        BrainfuckController.push(BrainfuckController.registerStart);
    }
}
