package CodeGenerating.Types;

/**
 * Enum of comparison operator types
 *
 * @author kacerekz
 */
public enum ComparatorType {
    // '<' | '>' | '>=' | '<=' | '!=' | '=='
    LESS_THAN,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN_OR_EQUAL,
    NOT_EQUAL,
    EQUAL
}
